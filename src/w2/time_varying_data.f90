!**********************************************************************
!*       s u b r o u t i n e   t i m e   v a r y i n g   d a t a       **
!***********************************************************************

!$message:'  subroutine time_varying_data'
subroutine time_varying_data (local_jday,local_nxtvd)
      use shared_data
      implicit none
      save

!****** type declaration NOT in common block

 
!<mueller>
!    <date>2001/02/06></date>
!    <reason>id's for netcd
!            nectgwdate nxqgw1 
!            and nxxqgw2 see time step 
!            conventions in manual
!            </reason>
       

        integer  unit !,     qssid    !mueller
        double precision     local_jday,   local_nxtvd

 !added after implicit none
        double precision ::  tdewnx, tdewo, windnx,phinx, phio, tairnx, tairo, cloudo
        double precision :: windo, cloudnx, ratio, tratio, cratio, hratio,  qratio
        integer :: local_jt,  local_jw, local_js, local_jo, local_k
        


!****** dimension declaration




!****** data declaration

        data unit /5330/
        unit = 5330


!****** open input files
        if (open_files) then
          met  = unit
          unit = unit+1
          open (met,file=metfn,status='old')
          read (met,*) 
          read (met,*)
          if (withdrawals) then
            wdq  = unit
            unit = unit+1
            open (wdq,file=qwdfn,status='old')
            read (wdq,1000)
          end if
          if (tributaries) then
            do local_jt=1,ntr
              trq(local_jt) = unit
              unit    = unit+1
              trt(local_jt) = unit
              unit    = unit+1
              open (trq(local_jt),file=qtrfn(local_jt),status='old')
              open (trt(local_jt),file=ttrfn(local_jt),status='old')
              read (trq(local_jt),1000)
              read (trt(local_jt),1000)
              if (constituents) then
                tribconc(local_jt) = unit
                unit    = unit+1
                open (tribconc(local_jt),file=ctrfn(local_jt),status='old')
                read (tribconc(local_jt),1000)
              end if
            end do
          end if
          do jb=1,nbp
            if (up_flow(jb)) then
              inq(jb) = unit
              unit    = unit+1
              intemp(jb) = unit
              unit    = unit+1
              open (inq(jb),file=qinfn(jb),status='old')
              open (intemp(jb),file=tinfn(jb),status='old')
              read (inq(jb),1000)
              read (intemp(jb),1000)
              if (constituents) then
                inc(jb) = unit
                unit    = unit+1
                open (inc(jb),file=cinfn(jb),status='old')
                read (inc(jb),1000)
              end if
            end if
            if (dn_flow(jb)) then
              otq(jb) = unit
              unit    = unit+1
              open (otq(jb),file=qotfn(jb),status='old')
              read (otq(jb),1000)
            end if
            if (precipitation) then
              pre(jb) = unit
              unit    = unit+1
              prt(jb) = unit
              unit    = unit+1
              open (pre(jb),file=prefn(jb),status='old')
              open (prt(jb),file=tprfn(jb),status='old')
              read (pre(jb),1000)
              read (prt(jb),1000)
              if (constituents) then
                prc(jb) = unit
                unit    = unit+1
                open (prc(jb),file=cprfn(jb),status='old')
                read (prc(jb),1000)
              end if
            end if
            if (dist_tribs(jb)) then
              dtq(jb) = unit
              unit    = unit+1
              dtt(jb) = unit
              unit    = unit+1
              open (dtq(jb),file=qdtfn(jb),status='old')
              open (dtt(jb),file=tdtfn(jb),status='old')
              read (dtq(jb),1000)
              read (dtt(jb),1000)
              if (constituents) then
                dtc(jb) = unit
                unit    = unit+1
                open (dtc(jb),file=cdtfn(jb),status='old')
                read (dtc(jb),1000)
              end if
            end if
            if (uh_external(jb)) then
              uhe(jb) = unit
              unit    = unit+1
              uht(jb) = unit
              unit    = unit+1
              open (uhe(jb),file=euhfn(jb),status='old')
              open (uht(jb),file=tuhfn(jb),status='old')
              read (uhe(jb),1000)
              read (uht(jb),1000)
              if (constituents) then
                uhc(jb) = unit
                unit    = unit+1
                open (uhc(jb),file=cuhfn(jb),status='old')
                read (uhc(jb),1000)
              end if
            end if
            if (dh_external(jb)) then
              dhe(jb) = unit
              unit    = unit+1
              dht(jb) = unit
              unit    = unit+1
              open (dhe(jb),file=edhfn(jb),status='old')
              open (dht(jb),file=tdhfn(jb),status='old')
              read (dhe(jb),1000)
              read (dht(jb),1000)
              if (constituents) then
                dhc(jb) = unit
                unit    = unit+1
                open (dhc(jb),file=cdhfn(jb),status='old')
                read (dhc(jb),1000)
              end if
            end if

          end do
          open_files = .false.
        end if
        local_nxtvd = 1.0e10

!****** meteorlogical data

        if (local_jday.ge.nxmet1) then
          do while (local_jday.ge.nxmet1)
            nxmet2 = nxmet1
            tdew   = tdewnx
            tdewo  = tdewnx
            wind   = windnx
            windo  = windnx
            phi    = phinx
            phio   = phinx
            tair   = tairnx
            tairo  = tairnx
            cloud  = cloudnx
            cloudo = cloudnx
            read (met,*) nxmet1,tairnx,tdewnx,windnx,phinx,cloudnx
            windnx = windnx*wsc(wscdp)
          end do
        end if
        local_nxtvd = min(local_nxtvd,nxmet1)
!****** withdrawals

        if (withdrawals) then
          if (local_jday.ge.nxqwd1) then
            do while (local_jday.ge.nxqwd1)
              nxqwd2 = nxqwd1
              do local_jw=1,nwd
                qwd(local_jw)  = qwdnx(local_jw)
                qwdo(local_jw) = qwdnx(local_jw)
              end do
              read (wdq,*) nxqwd1,(qwdnx(local_jw),local_jw=1,nwd)
            end do
          end if
          local_nxtvd = min(local_nxtvd,nxqwd1)
        end if
    


!****** tributaries

        if (tributaries) then
          do local_jt=1,ntr

!********** inflow

            if (local_jday.ge.nxqtr1(local_jt)) then
              do while (local_jday.ge.nxqtr1(local_jt))
                nxqtr2(local_jt) = nxqtr1(local_jt)
                qtr(local_jt)    = qtrnx(local_jt)
                qtro(local_jt)   = qtrnx(local_jt)
                read (trq(local_jt),*) nxqtr1(local_jt),qtrnx(local_jt)
              end do
            end if
            local_nxtvd = min(local_nxtvd,nxqtr1(local_jt))

!********** inflow temperatures

            if (local_jday.ge.nxttr1(local_jt)) then
              do while (local_jday.ge.nxttr1(local_jt))
                nxttr2(local_jt) = nxttr1(local_jt)
                ttr(local_jt)    = ttrnx(local_jt)
                ttro(local_jt)   = ttrnx(local_jt)
                read (trt(local_jt),*) nxttr1(local_jt),ttrnx(local_jt)
              end do
            end if
            local_nxtvd = min(local_nxtvd,nxttr1(local_jt))

!********** inflow constituent concentrations

            if (constituents) then
              if (local_jday.ge.nxctr1(local_jt)) then
                do while (local_jday.ge.nxctr1(local_jt))
                  nxctr2(local_jt) = nxctr1(local_jt)
                  do jc=1,nactr
                    ctr(trcn(jc),local_jt)  = ctrnx(trcn(jc),local_jt)
                    ctro(trcn(jc),local_jt) = ctrnx(trcn(jc),local_jt)
                  end do
                  read (tribconc(local_jt),*) nxctr1(local_jt),(ctrnx(trcn(jc),local_jt),    &
     &                                jc=1,nactr)
                end do
              end if
              local_nxtvd = min(local_nxtvd,nxctr1(local_jt))
            end if
          end do
        end if

!****** branch related inputs

        do jb=1,nbp

!******** inflow

          if (up_flow(jb)) then
            if (local_jday.ge.nxqin1(jb)) then
              do while (local_jday.ge.nxqin1(jb))
                nxqin2(jb) = nxqin1(jb)
                qin(jb)    = qinnx(jb)
                qino(jb)   = qinnx(jb)
                read (inq(jb),*) nxqin1(jb),qinnx(jb)
              end do
            end if
            local_nxtvd = min(local_nxtvd,nxqin1(jb))

!********** inflow temperature

            if (local_jday.ge.nxtin1(jb)) then
              do while (local_jday.ge.nxtin1(jb))
                nxtin2(jb) = nxtin1(jb)
                tin(jb)    = tinnx(jb)
                tino(jb)   = tinnx(jb)
                  
                    
                read (intemp(jb),*) nxtin1(jb),tinnx(jb)
              end do
            end if
            local_nxtvd = min(local_nxtvd,nxtin1(jb))

!********** inflow constituent concentrations

            if (constituents) then
              if (local_jday.ge.nxcin1(jb)) then
                do while (local_jday.ge.nxcin1(jb))
                  nxcin2(jb) = nxcin1(jb)
                  do jc=1,nacin
                    cin(incn(jc),jb)  = cinnx(incn(jc),jb)
                    cino(incn(jc),jb) = cinnx(incn(jc),jb)
                  end do
                  read (inc(jb),*) nxcin1(jb),(cinnx(incn(jc),jb),    &
     &                             jc=1,nacin)
                end do
              end if
              local_nxtvd = min(local_nxtvd,nxcin1(jb))
            end if
          end if

!******** outflow

          if (dn_flow(jb)) then
            if (local_jday.ge.nxqot1(jb)) then
              do while (local_jday.ge.nxqot1(jb))
                nxqot2(jb) = nxqot1(jb)
                if (sel_withdrawal(jb)) then
                  do local_js=1,nstr(jb)
                    qstr(local_js,jb)  = qstrnx(local_js,jb)
                    qstro(local_js,jb) = qstrnx(local_js,jb)
                  end do
                  read (otq(jb),*) nxqot1(jb),(qstrnx(local_js,jb),        &
     &                                local_js=1,nstr(jb))
                else
                  do local_jo=1,nout(jb)
                    qout(kout(local_jo,jb),jb)  = qoutnx(kout(local_jo,jb),jb)
                    qouto(kout(local_jo,jb),jb) = qoutnx(kout(local_jo,jb),jb)
                  end do
                  read (otq(jb),*) nxqot1(jb),(qoutnx(kout(local_jo,jb),    &
     &                                jb),local_jo=1,nout(jb))
                end if
              end do
            end if
            local_nxtvd = min(local_nxtvd,nxqot1(jb))
          end if

!******** distributed tributaries

          if (dist_tribs(jb)) then

!********** inflow

            if (local_jday.ge.nxqdt1(jb)) then
              do while (local_jday.ge.nxqdt1(jb))
                nxqdt2(jb) = nxqdt1(jb)
                qdtr(jb)   = qdtrnx(jb)
                qdtro(jb)  = qdtrnx(jb)
                read (dtq(jb),*) nxqdt1(jb),qdtrnx(jb)
              end do
            end if
            local_nxtvd = min(local_nxtvd,nxqdt1(jb))

!********** temperature

            if (local_jday.ge.nxtdt1(jb)) then
              do while (local_jday.ge.nxtdt1(jb))
                nxtdt2(jb) = nxtdt1(jb)
                tdtr(jb)   = tdtrnx(jb)
                tdtro(jb)  = tdtrnx(jb)
                read (dtt(jb),*) nxtdt1(jb),tdtrnx(jb)
              end do
            end if
            local_nxtvd = min(local_nxtvd,nxtdt1(jb))

!********** constituent concentrations

            if (constituents) then
              if (local_jday.ge.nxcdt1(jb)) then
                do while (local_jday.ge.nxcdt1(jb))
                  nxcdt2(jb) = nxcdt1(jb)
                  do jc=1,nacdt
                    cdtr(dtcn(jc),jb)  = cdtrnx(dtcn(jc),jb)
                    cdtro(dtcn(jc),jb) = cdtrnx(dtcn(jc),jb)
                  end do
                  read (dtc(jb),*) nxcdt1(jb),(cdtrnx(dtcn(jc),jb),    &
     &                                jc=1,nacdt)
                end do
              end if
              local_nxtvd = min(local_nxtvd,nxcdt1(jb))
            end if
          end if

!******** precipitation

          if (precipitation) then
            if (local_jday.ge.nxpr1(jb)) then
             do while (local_jday.ge.nxpr1(jb))
                nxpr2(jb) = nxpr1(jb)
                pr(jb)    = prnx(jb)
                read (pre(jb),*) nxpr1(jb),prnx(jb)
              end do
            end if
            local_nxtvd = min(local_nxtvd,nxpr1(jb))

!********** temperature

            if (local_jday.ge.nxtpr1(jb)) then
              do while (local_jday.ge.nxtpr1(jb))
                nxtpr2(jb) = nxtpr1(jb)
                tpr(jb)    = tprnx(jb)
                read (prt(jb),*) nxtpr1(jb),tprnx(jb)
              end do
            end if
            local_nxtvd = min(local_nxtvd,nxtpr1(jb))

!********** constituent concentrations

            if (constituents) then
              if (local_jday.ge.nxcpr1(jb)) then
                do while (local_jday.ge.nxcpr1(jb))
                  nxcpr2(jb) = nxcpr1(jb)
                  do jc=1,nacpr
                    cpr(prcn(jc),jb) = cprnx(prcn(jc),jb)
                  end do
                  read (prc(jb),*) nxcpr1(jb),(cprnx(prcn(jc),jb),    &
     &                                jc=1,nacpr)
                end do
              end if
              local_nxtvd = min(local_nxtvd,nxcpr1(jb))
            end if
          end if

!******** upstream head conditions

          if (uh_external(jb)) then

!********** elevations

            if (local_jday.ge.nxeuh1(jb)) then
              do while (local_jday.ge.nxeuh1(jb))
                nxeuh2(jb) = nxeuh1(jb)
                eluh(jb)   = eluhnx(jb)
                eluho(jb)  = eluhnx(jb)
                read (uhe(jb),*) nxeuh1(jb),eluhnx(jb)
              end do
            end if
            local_nxtvd = min(local_nxtvd,nxeuh1(jb))

!********** temperatures

            if (local_jday.ge.nxtuh1(jb)) then
              do while (local_jday.ge.nxtuh1(jb))
                nxtuh2(jb) = nxtuh1(jb)
                do local_k=2,kmp-1
                  tuh(local_k,jb)  = tuhnx(local_k,jb)
                  tuho(local_k,jb) = tuhnx(local_k,jb)
                end do
                read (uht(jb),*) nxtuh1(jb),(tuhnx(local_k,jb),local_k=2,kmp-1)
              end do
            end if
            local_nxtvd = min(local_nxtvd,nxtuh1(jb))

!********** constituent concentrations

            if (constituents) then
              if (local_jday.ge.nxcuh1(jb)) then
                do while (local_jday.ge.nxcuh1(jb))
                  nxcuh2(jb) = nxcuh1(jb)
                  do jc=1,nac
                    do local_k=2,kmp-1
                      cuh(local_k,cn(jc),jb)  = cuhnx(local_k,cn(jc),jb)
                      cuho(local_k,cn(jc),jb) = cuhnx(local_k,cn(jc),jb)
                    end do
                  end do
                  do jc=1,nac
                    read (uhc(jb),*) nxcuh1(jb),(cuhnx(local_k,cn(jc),jb), &
     &                                  local_k=2,kmp-1)
                  end do
                end do
              end if
              local_nxtvd = min(local_nxtvd,nxcuh1(jb))
            end if
          end if

!******** downstream head

          if (dh_external(jb)) then

!********** elevation

            if (local_jday.ge.nxedh1(jb)) then
              do while (local_jday.ge.nxedh1(jb))
                nxedh2(jb) = nxedh1(jb)
                eldh(jb)   = eldhnx(jb)
                eldho(jb)  = eldhnx(jb)
                read (dhe(jb),*) nxedh1(jb),eldhnx(jb)
              end do
            end if
            local_nxtvd = min(local_nxtvd,nxedh1(jb))

!********** temperature

            if (local_jday.ge.nxtdh1(jb)) then
              do while (local_jday.ge.nxtdh1(jb))
                nxtdh2(jb) = nxtdh1(jb)
                do local_k=2,kmp-1
                  tdh(local_k,jb)  = tdhnx(local_k,jb)
                  tdho(local_k,jb) = tdhnx(local_k,jb)
                end do
                read (dht(jb),*) nxtdh1(jb),(tdhnx(local_k,jb),local_k=2,kmp-1)
              end do
            end if
            local_nxtvd = min(local_nxtvd,nxtdh1(jb))

!********** constituents

            if (constituents) then
              if (local_jday.ge.nxcdh1(jb)) then
                do while (local_jday.ge.nxcdh1(jb))
                  nxcdh2(jb) = nxcdh1(jb)
                  do jc=1,nac
                    do local_k=2,kmp-1
                      cdh(local_k,cn(jc),jb)  = cdhnx(local_k,cn(jc),jb)
                      cdho(local_k,cn(jc),jb) = cdhnx(local_k,cn(jc),jb)
                    end do
                  end do
                  do jc=1,nac
                    read (dhc(jb),*) nxcdh1(jb),(cdhnx(local_k,cn(jc),jb), &
     &                                  local_k=2,kmp-1)
                  end do
                end do
              end if
              local_nxtvd = min(local_nxtvd,nxcdh1(jb))
            end if
          end if
        end do

!****** dead sea case

        if (no_wind) then
          wind   = 0.0d0
          windo  = 0.0d0
          windnx = 0.0d0
        end if
        if (no_inflow) then
          do jb=1,nbp
            qin(jb)    = 0.0d0
            qino(jb)   = 0.0d0
            qinnx(jb)  = 0.0d0
            qdtr(jb)   = 0.0d0
            qdtro(jb)  = 0.0d0
            qdtrnx(jb) = 0.0d0
            pr(jb)     = 0.0d0
            prnx(jb)   = 0.0d0
          end do
          do local_jt=1,ntr
            qtr(local_jt)   = 0.0d0
            qtro(local_jt)  = 0.0d0
            qtrnx(local_jt) = 0.0d0
          end do
        end if
        if (no_outflow) then
          do jb=1,nbp
            if (sel_withdrawal(jb)) then
              do local_js=1,nstr(jb)
                qstr(local_js,jb)   = 0.0d0
                qstro(local_js,jb)  = 0.0d0
                qstrnx(local_js,jb) = 0.0d0
              end do
            else
              do local_k=kt,kb(id)
                qout(local_k,jb)   = 0.0d0
                qouto(local_k,jb)  = 0.0d0
                qoutnx(local_k,jb) = 0.0d0
              end do
            end if
          end do
          do local_jw=1,nwd
            qwd(local_jw)   = 0.0d0
            qwdo(local_jw)  = 0.0d0
            qwdnx(local_jw) = 0.0d0
          end do
        end if

!****** input formats

 1000   format(//)
 1010   format(10f8.0)
 1020   format(10f8.0:/(8x,9f8.0))
 1030   format(21f8.0)
      return

!**********************************************************************
!*                   i n t e r p o l a t e  i n p u t s              **
!**********************************************************************

      entry interpolate_inputs (local_jday)

!****** meteorlogical data

        if (interp_met) then
          ratio = (nxmet1-local_jday)/(nxmet1-nxmet2)
          tdew  = (1.0-ratio)*tdewnx+ratio*tdewo
          wind  = (1.0-ratio)*windnx+ratio*windo
          phi   = (1.0-ratio)*phinx+ratio*phio
          tair  = (1.0-ratio)*tairnx+ratio*tairo
          cloud = (1.0-ratio)*cloudnx+ratio*cloudo
        end if

!****** withdrawals

        if (withdrawals) then
          if (interp_withdrwl) then
            qratio = (nxqwd1-local_jday)/(nxqwd1-nxqwd2)
            do local_jw=1,nwd
              qwd(local_jw) = (1.0-qratio)*qwdnx(local_jw)+qratio*qwdo(local_jw)
            end do
          end if
        end if

!****** tributaries

        if (tributaries) then
          if (interp_tribs) then
            do local_jt=1,ntr
              qratio = (nxqtr1(local_jt)-local_jday)/(nxqtr1(local_jt)-nxqtr2(local_jt))
              tratio = (nxttr1(local_jt)-local_jday)/(nxttr1(local_jt)-nxttr2(local_jt))
              if (constituents) then
                cratio = (nxctr1(local_jt)-local_jday)/(nxctr1(local_jt)-nxctr2(local_jt))
              end if
              qtr(local_jt) = (1.0-qratio)*qtrnx(local_jt)+qratio*qtro(local_jt)
              ttr(local_jt) = (1.0-tratio)*ttrnx(local_jt)+tratio*ttro(local_jt)
              do jc=1,nactr
                ctr(trcn(jc),local_jt) = (1.0-cratio)*ctrnx(trcn(jc),local_jt)        &
     &                             +cratio*ctro(trcn(jc),local_jt)
              end do
            end do
          end if
        end if

!****** branch related inputs

        do jb=1,nbp

!******** inflow

          if (up_flow(jb)) then
            if (interp_inflow) then
              qratio = (nxqin1(jb)-local_jday)/(nxqin1(jb)-nxqin2(jb))
              tratio = (nxtin1(jb)-local_jday)/(nxtin1(jb)-nxtin2(jb))
              if (constituents) then
                cratio = (nxcin1(jb)-local_jday)/(nxcin1(jb)-nxcin2(jb))
              end if
              qin(jb) = (1.0-qratio)*qinnx(jb)+qratio*qino(jb)
              tin(jb) = (1.0-tratio)*tinnx(jb)+tratio*tino(jb)
              do jc=1,nacin
                cin(incn(jc),jb) = (1.0-cratio)*cinnx(incn(jc),jb)        &
     &                             +cratio*cino(incn(jc),jb)
              end do
            end if
          end if

!******** outflow

          if (dn_flow(jb)) then
            if (interp_outflow) then
              qratio = (nxqot1(jb)-local_jday)/(nxqot1(jb)-nxqot2(jb))
              if (sel_withdrawal(jb)) then
                do local_js=1,nstr(jb)
                  qstr(local_js,jb) = (1.0-qratio)*qstrnx(local_js,jb)                &
     &                          +qratio*qstro(local_js,jb)
                end do
              else
                qsum(jb) = 0.0d0
                do local_jo=1,nout(jb)
                  local_k          = kout(local_jo,jb)
                  qout(local_k,jb) = (1.0-qratio)*qoutnx(local_k,jb)+qratio            &
     &                         *qouto(local_k,jb)
                  qsum(jb)   = qsum(jb)+qout(local_k,jb)
                end do
              end if
            end if
          end if

!******** distributed tributaries

          if (dist_tribs(jb)) then
            if (interp_dtribs) then
              qratio = (nxqdt1(jb)-local_jday)/(nxqdt1(jb)-nxqdt2(jb))
              tratio = (nxtdt1(jb)-local_jday)/(nxtdt1(jb)-nxtdt2(jb))
              if (constituents) then
                cratio = (nxcdt1(jb)-local_jday)/(nxcdt1(jb)-nxcdt2(jb))
              end if
              qdtr(jb) = (1.0-qratio)*qdtrnx(jb)+qratio*qdtro(jb)
              tdtr(jb) = (1.0-tratio)*tdtrnx(jb)+tratio*tdtro(jb)
              do jc=1,nacdt
                cdtr(dtcn(jc),jb) = (1.0-cratio)*cdtrnx(dtcn(jc),jb)    &
     &                              +cratio*cdtro(dtcn(jc),jb)
              end do
            end if
          end if

!******** upstream head conditions

          if (uh_external(jb)) then
            if (interp_head) then
              hratio = (nxeuh1(jb)-local_jday)/(nxeuh1(jb)-nxeuh2(jb))
              tratio = (nxtuh1(jb)-local_jday)/(nxtuh1(jb)-nxtuh2(jb))
              if (constituents) then
                cratio = (nxcuh1(jb)-local_jday)/(nxcuh1(jb)-nxcuh2(jb))
              end if
              eluh(jb) = (1.0-hratio)*eluhnx(jb)+hratio*eluho(jb)
              do local_k=2,kmp-1
                tuh(local_k,jb) = (1.0-tratio)*tuhnx(local_k,jb)+tratio*tuho(local_k,jb)
              end do
              do jc=1,nac
                do local_k=2,kmp-1
                  cuh(local_k,cn(jc),jb) = (1.0-cratio)*cuhnx(local_k,cn(jc),jb)    &
     &                               +cratio*cuho(local_k,cn(jc),jb)
                end do
              end do
            end if
          end if

!******** downstream head

          if (dh_external(jb)) then
            if (interp_head) then
              hratio = (nxedh1(jb)-local_jday)/(nxedh1(jb)-nxedh2(jb))
              tratio = (nxtdh1(jb)-local_jday)/(nxtdh1(jb)-nxtdh2(jb))
              if (constituents) then
                cratio = (nxcdh1(jb)-local_jday)/(nxcdh1(jb)-nxcdh2(jb))
              end if
              eldh(jb) = (1.0-hratio)*eldhnx(jb)+hratio*eldho(jb)
              do local_k=2,kmp-1
                tdh(local_k,jb) = (1.0-tratio)*tdhnx(local_k,jb)+tratio*tdho(local_k,jb)
              end do
              do jc=1,nac
                do local_k=2,kmp-1
                  cdh(local_k,cn(jc),jb) = (1.0-cratio)*cdhnx(local_k,cn(jc),jb)    &
     &                               +cratio*cdho(local_k,cn(jc),jb)
                end do
              end do
            end if
          end if
        end do
      end

