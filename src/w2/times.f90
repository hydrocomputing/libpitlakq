!***********************************************************************
!*              s u b r o u t i n e   d a t e   t i m e               **
!***********************************************************************

!$message:'  subroutine date_time'

subroutine date_time(cdate, c_time)
      !use ifport     
      implicit none
      character*8  cdate, c_time
      integer:: datetime(8), iyr
      character*10 b(3)
      call date_and_time(b(1), b(2), b(3), datetime)
      iyr = datetime(1)
      if (iyr.ge.2000) then 
        iyr = iyr -2000
      else 
        iyr = iyr -1900
      endif
      write (cdate,"(i2,'/',i2,'/',i2)") datetime(2), datetime(3), iyr
      write (c_time,"(i2,':',i2,':',i2)") datetime(5), datetime(6), datetime(7)
end


!subroutine date_time (cdate,c_time)
!      !use ifport     
!      implicit none
!!****** type declaration
!        integer :: iyr, imon, iday, ihr, imin, isec, ihund
!        character*8  cdate, c_time                                       !fortran
!
!        call getdat (iyr,imon,iday)                                     !fortran
!        call gettim (ihr,imin,isec,ihund)                                  !fortran
!!<mueller>  
!        !<date>2001/02/06</date>
!        !<reason>date needs to be 4 digiits in order 
!        !to fit character length</reason>                            
!      if (iyr.ge.2000) then 
!        iyr = iyr -2000
!      else 
!        iyr = iyr -1900
!      endif
!!</mueller>
!        write (cdate,"(i2,'/',i2,'/',i2)") imon, iday, iyr              !fortran
!        write (c_time,"(i2,':',i2,':',i2)") ihr,  imin, isec             !fortran
!      end

!***********************************************************************
!*         s u b r o u t i n e    g r e g o r i a n   d a t e         **
!***********************************************************************

!$message:'  subroutine gregorian_date'
      subroutine gregorian_date (local_year)
      use shared_data
      implicit none
      
        integer :: local_year

!****** determine if new year (regular or leap) and increment year

        if (.not.leap_year.and.jdayg.ge.366) then
          jdayg     = jdayg-365
          local_year      = local_year+1
          leap_year = mod(local_year,4).eq.0
        else if (jdayg.ge.367) then
          jdayg     = jdayg-366
          local_year      = local_year+1
          leap_year = .false.
        end if

!****** determine month and day of year

        if (leap_year) then
          if (jdayg.ge.1.and.jdayg.lt.32) then
            gday  = jdayg
            daym  = 31.0
            month = '  january'
          else if (jdayg.ge.32.and.jdayg.lt.61) then
            gday  = jdayg-31
            daym  = 29.0
            month = ' february'
          else if (jdayg.ge.61.and.jdayg.lt.92) then
            gday  = jdayg-60
            daym  = 31.0
            month = '    march'
          else if (jdayg.ge.92.and.jdayg.lt.122) then
            gday  = jdayg-91
            daym  = 30.0
            month = '    april'
          else if (jdayg.ge.122.and.jdayg.lt.153) then
            gday  = jdayg-121
            daym  = 31.0
            month = '      may'
          else if (jdayg.ge.153.and.jdayg.lt.183) then
            gday  = jdayg-152
            daym  = 30.0
            month = '     june'
          else if (jdayg.ge.181.and.jdayg.lt.214) then
            gday  = jdayg-182
            daym  = 31.0
            month = '     july'
          else if (jdayg.ge.214.and.jdayg.lt.245) then
            gday  = jdayg-213
            daym  = 31.0
            month = '   august'
          else if (jdayg.ge.245.and.jdayg.lt.275) then
            gday  = jdayg-244
            daym  = 30.0
            month = 'september'
          else if (jdayg.ge.275.and.jdayg.lt.306) then
            gday  = jdayg-274
            daym  = 31.0
            month = '  october'
          else if (jdayg.ge.306.and.jdayg.lt.336) then
            gday  = jdayg-305
            daym  = 30.0
            month = ' november'
          else if (jdayg.ge.336.and.jdayg.lt.367) then
            gday  = jdayg-335
            daym  = 31.0
            month = ' december'
          end if
        else
          if (jdayg.ge.1.and.jdayg.lt.32) then
            gday  = jdayg
            daym  = 31.0
            month = '  january'
          else if (jdayg.ge.32.and.jdayg.lt.60) then
            gday  = jdayg-31
            daym  = 29.0
            month = ' february'
          else if (jdayg.ge.60.and.jdayg.lt.91) then
            gday  = jdayg-59
            daym  = 31.0
            month = '    march'
          else if (jdayg.ge.91.and.jdayg.lt.121) then
            gday  = jdayg-90
            daym  = 30.0
            month = '    april'
          else if (jdayg.ge.121.and.jdayg.lt.152) then
            gday  = jdayg-120
            daym  = 31.0
            month = '      may'
          else if (jdayg.ge.152.and.jdayg.lt.182) then
            gday  = jdayg-151
            daym  = 30.0
            month = '     june'
          else if (jdayg.ge.182.and.jdayg.lt.213) then
            gday  = jdayg-181
            daym  = 31.0
            month = '     july'
          else if (jdayg.ge.213.and.jdayg.lt.244) then
            gday  = jdayg-212
            daym  = 31.0
            month = '   august'
          else if (jdayg.ge.244.and.jdayg.lt.274) then
            gday  = jdayg-243
            daym  = 30.0
            month = 'september'
          else if (jdayg.ge.274.and.jdayg.lt.305) then
            gday  = jdayg-273
            daym  = 31.0
            month = '  october'
          else if (jdayg.ge.305.and.jdayg.lt.335) then
            gday  = jdayg-304
            daym  = 30.0
            month = ' november'
          else if (jdayg.ge.335.and.jdayg.lt.366) then
            gday  = jdayg-334
            daym  = 31.0
            month = ' december'
          end if
        end if
      end
