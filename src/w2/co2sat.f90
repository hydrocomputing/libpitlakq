double precision function co2SatANN(pCO2, temp, pH)

integer :: i, j
double precision, intent(in) :: pCO2
double precision, intent(in) :: temp
double precision, intent(in) :: pH
double precision, dimension(3) :: input
double precision, dimension(4) :: maxima
double precision, dimension(3,4) :: wi
double precision, dimension(3) :: wo
double precision, dimension(3) :: ah                    ! hidden activations
double precision :: sum
double precision, dimension(4) :: ai                    ! inout activation



if(pH > 8) then
    maxima = (/0.0005000000000000d0, 30.0000000000000000d0, 9.1769400000000001d0, 0.0090977000000000d0/)
    wi = reshape((/ &
               -0.6963860326821334d0, 3.6882724272832661d0, -2.3914130303527221d0, &
                0.4948450741277209d0, -1.2857854697107889d0, -4.1983482636895912d0, &
                -28.5002436208018640d0, 75.0185285068655930d0, -1.5954251050618757d0, &
                24.9345497528045780d0, -78.4190215102405830d0, -1.2039046736647467d0 &
                /), (/3,4/))
    wo = (/-5.6356880474069317d0, 38.8885009056101720d0,        -1.464010d0/)
else if(pH > 7) then
    maxima = (/0.0005000000000000d0, 30.0000000000000000d0, 7.9999900000000004d0, 0.0011597000000000d0/)
    wi = reshape((/ &
               -0.4585881985579851d0, 1.8700448491228447d0, 1.4295700403203055d0, &
                0.2146032588429606d0, -2.6634734202459285d0, -0.6444278631320364d0, &
                -7.4033147254721170d0, -2.9248670604825482d0, 23.7735556832544890d0, &
                6.2730622649817516d0, -1.0911202946996266d0, -26.8436163175685320d0 &
                /), (/3,4/))
    wo = (/-6.0964330621107443d0, -2.0017796762901687d0,        21.525248d0/)
else if(pH > 6) then
    maxima = (/0.0005000000000000d0, 30.0000000000000000d0, 6.9999200000000004d0, 0.0001435700000000d0/)
    wi = reshape((/ &
               -1.2306113620308303d0, 0.7551414301072136d0, -1.7039384596334792d0, &
                0.4690565135504441d0, -0.7870493102609329d0, -1.1358293441166418d0, &
                -18.6535277727488340d0, 1.4631833895168713d0, -1.9510727064341029d0, &
                21.0279367674436700d0, 1.3002497967537880d0, -1.4701646355930886d0 &
                /), (/3,4/))
    wo = (/-12.9264739237653750d0, 12.0681826144053160d0,        -0.581801d0/)
else if(pH > 5) then
    maxima = (/0.0005000000000000d0, 30.0000000000000000d0, 5.9990100000000002d0, 0.0000453590000000d0/)
    wi = reshape((/ &
               -4.2651753746914931d0, 1.3702162495145238d0, -1.8135109958091029d0, &
                3.3565401218689401d0, -0.8521466592778648d0, -0.6322366078049594d0, &
                -4.4870266814234405d0, 2.3412885766099176d0, -1.7345288138516923d0, &
                9.0809661842483251d0, -0.8738773724041808d0, -1.6083808052718755d0 &
                /), (/3,4/))
    wo = (/-6.9263021203811919d0, 7.4599802441954779d0,        -0.130133d0/)
else if(pH > 4) then
    maxima = (/0.0005000000000000d0, 30.0000000000000000d0, 4.9984500000000001d0, 0.0000396470000000d0/)
    wi = reshape((/ &
               -4.6494461206204960d0, 1.2845516566739521d0, -1.6098766421983173d0, &
                4.8406813545465894d0, -0.8543720166166779d0, -1.2843750858293213d0, &
                -0.3136471795429887d0, 0.2010087034614498d0, -1.7075634822620307d0, &
                5.1321255275150053d0, 0.4711767073143250d0, -1.2924620643481519d0 &
                /), (/3,4/))
    wo = (/-5.3006796983690867d0, 6.4188371536283944d0,        -0.835154d0/)
else if(pH > 3) then
    maxima = (/0.0005000000000000d0, 30.0000000000000000d0, 3.9996800000000001d0, 0.0000390120000000d0/)
    wi = reshape((/ &
               -5.2412920468871285d0, 1.0945745537220077d0, -1.6627122460752535d0, &
                5.7075740464154778d0, -0.7686216965979991d0, -1.2790304103472854d0, &
                -0.0518036491089416d0, 0.0120487641060303d0, -1.7565415139394513d0, &
                5.6695513813358502d0, 0.7594468460529628d0, -1.3438594916009929d0 &
                /), (/3,4/))
    wo = (/-6.5181568834214927d0, 8.0486969313917971d0,        -0.771338d0/)
else
    maxima = (/0.0005000000000000d0, 30.0000000000000000d0, 2.9998600000000000d0, 0.0000389200000000d0/)
    wi = reshape((/ &
               -5.7670654779769359d0, 0.9929575581851999d0, -1.7260824008366931d0, &
                6.6172875464876029d0, -0.7257858504648224d0, -1.2608078581160993d0, &
                -0.0511788575197024d0, -0.0095933457777149d0, -1.7871464380691000d0, &
                6.3612827752072310d0, 0.9388192795277817d0, -1.3900348971948846d0 &
                /), (/3,4/))
    wo = (/-7.9596593851156490d0, 9.7428113834146277d0,        -0.709437d0/)
end if

input(1) = pCO2
input(2) = temp
input(3) = pH
ai(1:3) = input/maxima(1:3)
ai(4) = 1.0

!# hidden activations
do i=1,3        !nh
    sum = 0.0d0
    do j=1,4 !ni
       sum = sum + ai(j) * wi(i,j)
    end do
    ah(i) = 1.0/(1.0+exp(-sum))
end do

!# output activations
sum = 0.0d0
do j = 1,3 ! nh
   sum = sum + ah(j) * wo(j)
end do

co2SatANN = (1.0/(1.0+exp(-sum))*maxima(4))*12011.1


end function