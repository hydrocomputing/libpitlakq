    subroutine deallocate_main 
      use shared_data
      
      deallocate(cunit3)
      deallocate(acc, inacc, tracc, dtacc, pracc)
      deallocate(dtrc, swc) 
      deallocate(cunit2)
      deallocate(cname1)
      deallocate(segment)
      deallocate(trc)
!      deallocate(rsifn,  vprfn(nbp), tsrfn(nbp), prffn(nbp), &
!     &               vplfn(nbp), cplfn(nbp), snpfn(nbp),    &
!     &             lprfn(nbp),  bthfn(nbp), sprfn(nbp))
      deallocate(sink)
      deallocate(conv1)    
    


      !deallocate (cn0)
      deallocate (skti, kbmin)
      deallocate (iprf, ispr)    
      deallocate (ipri)
      deallocate (iwd, kwd, jbwd)
      deallocate (jbtr, itr, kttr, kbtr) 
      deallocate (ktqin, kbqin)
      deallocate (jbuh, jbdh)
      deallocate (uhs, dhs, us, nl)


      deallocate (dn_head,up_head)
      deallocate (ice_in) 
      deallocate (allow_ice,    one_layer)
      deallocate (iso_conc, vert_conc, long_conc)                    
      deallocate (transport)
      deallocate (place_qtr, specify_qtr)


!**** dimension statements

   
      deallocate (icesw)
      deallocate (bhrkt2, avhkt,  dlx, dlxr, elws)      
      deallocate (dlxrho)
      deallocate (ev, qdt, qpr)
      deallocate (q, qc, qssum)
      deallocate (sods)
      deallocate (rn, phi0)
      deallocate (avh)
      !<mueller>
!    <date>2001/02/06></date>
!    <reason>void</reason>    
      deallocate (w2z)
!</mueller>
      deallocate (depthb)
      deallocate (tvp)
      deallocate (sf1v)
      deallocate (twd) 
      deallocate (eltrt, eltrb)
      deallocate (c2i, avcout)
      deallocate (qprbr,  evbr)    
      deallocate (snpd, vpld, prfd, cpld, rsod, tsrd, dltd, &
     &                          wscd, sprd, scrd)
      deallocate (snpf, vplf, prff, cplf, rsof, tsrf,  &
     &                          dltf, scrf, sprf)
      deallocate (dltmax)  
!<mueller>
!    <date>2001/02/06></date>
!    <reason>void</reason>
      deallocate (w2x)
      
      deallocate (sf2v, sf3v, sf4v, sf5v, sf6v,            &
     &          sf7v, sf8v, sf9v, sf10v)
      deallocate (xdummy)
!</mueller>    

      deallocate (vactive)
      deallocate (hactive)
      deallocate (balance_error)
      !<mueller>
!    <date>201/02/06</date>
!    <reason>gw-inflow for all cells</reason>
      deallocate (qgw)    
      deallocate (qgwin) 
      deallocate (qgwout) 
!    <date>2005-05-13</date>
!    <reason>gw and lake temperature for all 
      deallocate (tgw)
      deallocate (tlake) 
      deallocate (currentGwOutVolume)    
      !<date>2013/08/20</date>
      deallocate (tload)   
!</mueller>    

      deallocate (admx, admz, dz, dx, &
     &             dm, p,    &
     &             hpg, hdg, sb, st,    dzq)
      deallocate (su, sw, saz)
      deallocate (tss, qss)
      deallocate (bb, br, bhr, hseg) 
      deallocate (sf1l)
      deallocate (cwd)
      deallocate (quh1, quh2, qdh1, qdh2)
      deallocate (quh1_cum_plus, quh2_cum_plus, qdh1_cum_plus, qdh2_cum_plus)
      deallocate (quh1_cum_minus, quh2_cum_minus, qdh1_cum_minus, qdh2_cum_minus)
      deallocate (tssuh1 , tssuh2 , tssdh1 , tssdh2 )
      deallocate (qinf)    
      deallocate (qtrf)
      deallocate (akbr)
      deallocate (fetchu, fetchd)
      deallocate (cvp)
      deallocate (sf2l, sf3l, sf4l, sf5l, sf6l,        &
     &          sf7l, sf8l, sf9l, sf10l, &
     &            sf11l, sf12l, sf13l)
      deallocate (cssuh1 , cssuh2 , cssdh1 , cssdh2) 
      deallocate (c1s,  cssb)

    

!<mueller>
!    <date>2001/02/06</date>
!    <reason> keeping track of gw volume</reason>
      deallocate (volgwin, volgwout, tssgwin, tssgwout, volload, tssload)
      deallocate (tssev,  tsspr,  tsstr,        &
                tssdt,  tsswd,  tssuh,    &
     &          tssdh,  tssin,  tssout, tsss, &
     &            tssb,   tssice,    &
     &          esbr,   etbr,   eibr)
      deallocate (volsbr, voltbr, volev, volpr,    &
            voltr, voldt, volwd,  voluh,  voldh,   &
    &        volin,  volout, dlvol, volibr)
      deallocate (bhrho, gma, bta, a, c, d,        &
     &                                       v, f)
      deallocate (sz)
      deallocate (gmat,   dt,     vt)
      deallocate (btat,    ct,  at)
      deallocate (tadl,   tadv)
      deallocate (ad1l,   ad2l,   ad3l,        &
     &            ad1v,   ad2v,   ad3v)
      deallocate (dx1,    dx2,    dx3)
      deallocate (cmbrs,  cmbrt)
      deallocate (cadl,  cadv)
      deallocate (csstr, cssdt, csswd, csspr,     &
                  cssin, cssout, cssuh, cssdh,    &
                  cssphc, cssw2, cssgwin, cssgwout, &
                  cssatm, csssed, cssload)


    end subroutine deallocate_main