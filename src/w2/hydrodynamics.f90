    subroutine hydrodynamics( )
        use shared_data
        implicit none
          integer :: i_add
          double precision :: density
          include 'formats_main.inc'
!if (year > 2006 .or. jday < 1.3) write(*,*) 't1 in hydrodynamics begin', t1
!write(*,*) 'hydrodynamics'
!write(*,*) 'volev hydro begin', volev
!write(*,*)
!write(*,*) volsbr, voltbr, volev, volpr, voldt, volwd, voluh, voldh, volin, volout, dlvol, volibr
!write(*,*)
!write(*,*) 'voltr hydro begin', voltr
        if (jday.ge.nxtvd) then
            call time_varying_data(jday,nxtvd)
        end if

        if (interpolate)   call interpolate_inputs (jday)
        dlttvds = dlttvd
        dlttvd  = int((nxtvd-jday)*86400.0)+1.0d0

!***********************************************************************
!*                 task 2.1: hydrodynamic sources/sinks               **
!***********************************************************************
!write(*,*) 'hydro'
!$message:'    hydrodynamics'
!$message:'      sources/sinks'
        do jb=1,nbp
          iu = cus(jb)
          id = ds(jb)
          if (sel_withdrawal(jb)) call selective_withdrawal
        end do



!****** timestep violation entry point

10010   continue
        do jb=1,nbp
          iu = cus(jb)
          id = ds(jb)
          if (evaporation) then
            evbr(jb) = 0.0d0
            fw       = 9.2+0.46*wind*wind
            do i=iu,id
              tm    = (t2(kt,i)+tdew)*0.5d0
              vptg  = 0.35+0.015*tm+0.0012*tm*tm
              ev(i) = vptg*(t2(kt,i)-tdew)*fw*b(kt,i)*dlx(i)/2.45e9
              if (ev(i).lt.0.0.or.ice(i)) ev(i) = 0.0d0
              qss(kt,i) = qss(kt,i)-ev(i)
              evbr(jb)  = evbr(jb)+ev(i)
            end do
          end if
          if (precipitation) then
            qprbr(jb) = 0.0d0
            do i=iu,id
              qpr(i)    = pr(jb)*b(kti(i),i)*dlx(i)
              qprbr(jb) = qprbr(jb)+qpr(i)
              qss(kt,i) = qss(kt,i)+qpr(i)
            end do
          end if

          if (tributaries) then
            do jt=1,ntr

!************ inflow fractions

              if (jb.eq.jbtr(jt)) then
                i = max(itr(jt),iu)
                do k=kt,kb(i)
                  qtrf(k,jt) = 0.0d0
                end do
                if (place_qtr(jt)) then

!**************** inflow layer

                  k     = kt
                  rhotr = density (ttr(jt),ctr(2,jt),ctr(4,jt))
                  do while (rhotr.gt.rho(k,i).and.k.lt.kb(i))
                    k = k+1
                  end do
                  kttr(jt) = k
                  kbtr(jt) = k

!**************** layer inflows

                  vqtr  = qtr(jt)*dlt
                  vqtri = vqtr
                  qtrfr = 1.0d0
                  incr  = -1
                  do while (qtrfr.gt.0.0)
                    if (k.le.kb(i)) then
                      vol = bh(k,i)*dlx(i)
                      if (k.eq.kt) vol = bhkt2(i)*dlx(i)
                      if (vqtr.gt.0.5d0*vol) then
                        qtrf(k,jt) = 0.5d0*vol/vqtri
                        qtrfr      = qtrfr-qtrf(k,jt)
                        vqtr       = vqtr-qtrf(k,jt)*vqtri
                        if (k.eq.kt) then
                          k    = kbtr(jt)
                          incr = 1
                        end if
                      else
                        qtrf(k,jt) = qtrfr
                        qtrfr      = 0.0d0
                      end if
                      if (incr.lt.0) kttr(jt) = k
                      if (incr.gt.0) kbtr(jt) = min(kb(i),k)
                      k = k+incr
                    else
                      qtrf(kt,jt) = qtrfr
                      qtrfr       = 0.0d0
                    end if
                  end do
                else
                  if (specify_qtr(jt)) then
                    kttr(jt) = 2
                    do while (el(kttr(jt)).gt.eltrt(jt))
                      kttr(jt) = kttr(jt)+1
                    end do
                    kttr(jt) = kttr(jt)-1
                    kbtr(jt) = kb(i)
                    do while (el(kbtr(jt)).lt.eltrb(jt))
                      kbtr(jt) = kbtr(jt)-1
                    end do
                  else
                    kttr(jt) = kt
                    kbtr(jt) = kb(i)
                  end if
                  kttr(jt) = max(kt,kttr(jt))
                  kbtr(jt) = min(kb(i),kbtr(jt))
                  bhsum    = 0.0d0
                  do k=kttr(jt),kbtr(jt)
                    bht = bh(k,i)
                    if (k.eq.kt) bht = bhkt2(i)
                    bhsum = bhsum+bht
                  end do
                  do k=kttr(jt),kbtr(jt)
                    bht = bh(k,i)
                    if (k.eq.kt) bht = bhkt2(i)
                    qtrf(k,jt) = bht/bhsum
                  end do
                end if
                do k=kttr(jt),kbtr(jt)
                  qss(k,i) = qss(k,i)+qtr(jt)*qtrf(k,jt)
                end do
              end if
            end do
          end if
          if (dist_tribs(jb)) then
            do i=iu,id
              qdt(i)    = qdtr(jb)*b(kt,i)*dlx(i)/akbr(kt,jb)
              qss(kt,i) = qss(kt,i)+qdt(i)
            end do
          end if
          if (withdrawals) then
            do jw=1,nwd
              if (jb.eq.jbwd(jw)) then
                i        = max(cus(jbwd(jw)),iwd(jw))
                k        = max(kt,kwd(jw))
                qss(k,i) = qss(k,i)-qwd(jw)
              end if
            end do
          end if
          if (uh_internal(jb)) then
            do k=kt,kb(iu-1)
              qss(k,uhs(jb)) = qss(k,uhs(jb))-quh2(k,jb)/dlt
            end do
          end if
          if (dh_internal(jb)) then
            do k=kt,kb(id+1)
              qss(k,dhs(jb)) = qss(k,dhs(jb))+qdh2(k,jb)/dlt
            end do
          end if
!<mueller>
!    <date>2001/02/06></date>
!    <reason>adding gw inflow to all elements -> mixing in equally
!            moving gw to layer with nearest density might be worth
!            considering as an option
!    </reason>

        if_gw_coupling: if (gw_coupling) then
            do_all_segments: do i = iu, id
                do_all_layers: do k =kt, kb(i)
                    qss(k,i) = qss(k,i) + qgw(k,i)
                end do do_all_layers
            end do do_all_segments
        end if if_gw_coupling

        !<date>2013/08/20</date>
        if (loading) then
            do i = iu, id
                do k =kt, kb(i)
                    qss(k,i) = qss(k,i) + qload(k,i)
                end do
            end do
        end if
!</mueller>
        end do

!***********************************************************************
!*               task 2.2: hydrodynamic calculations                  **
!***********************************************************************
!write(*,*) 'calc'
!$message:'      calculations'
!$message:'        boundary concentrations, temperatures, and densities'
        do jb=1,nbp
          iu = cus(jb)
          id = ds(jb)

!***********************************************************************
!*  task 2.2.1: boundary concentrations, temperatures, and densities  **
!***********************************************************************

          iut = iu
          idt = id
          if (up_flow(jb)) then
            do k=kt,kb(iu)
              do jc=1,nac
                c1s(k,iu-1,cn(jc)) = cin(cn(jc),jb)
              end do
              if (qin(jb).gt.0.0) then
                t1(k,iu-1) = tin(jb)
                t2(k,iu-1) = tin(jb)
              else
                t1(k,iu-1) = t1(k,iu)
                t2(k,iu-1) = t2(k,iu)
              end if
            end do
          end if
          if (dn_flow(jb)) then
            do k=kt,kb(iu)
              do jc=1,nac
                c1s(k,id+1,cn(jc)) = c1s(k,id,cn(jc))
              end do
              t1(k,id+1) = t2(k,id)
              t2(k,id+1) = t2(k,id)
            end do
          end if
          if (up_head(jb)) then
            iut = iu-1
            if (uh_internal(jb)) then
              do k=kt,kb(iut)
                do jc=1,nac
                  c1s(k,iut,cn(jc)) = c1s(k,uhs(jb),cn(jc))
                  c1(k,iut,cn(jc))  = c1s(k,uhs(jb),cn(jc))
                  c2(k,iut,cn(jc))  = c1s(k,uhs(jb),cn(jc))
                end do
                t1(k,iut)  = t2(k,uhs(jb))
                t2(k,iut)  = t2(k,uhs(jb))
                rho(k,iut) = rho(k,uhs(jb))
              end do
            else if (uh_external(jb)) then
              do k=kt,kb(iut)
                do jc=1,nac
                  c1s(k,iut,cn(jc)) = cuh(k,cn(jc),jb)
                  c1(k,iut,cn(jc))  = cuh(k,cn(jc),jb)
                  c2(k,iut,cn(jc))  = cuh(k,cn(jc),jb)
                end do
                t1(k,iut)  = tuh(k,jb)
                t2(k,iut)  = tuh(k,jb)
                if (constituents) then
                  rho(k,iut) = density (t2(k,iut),ss(k,iut),tds(k,iut))
                else
                  rho(k,iut) = density (t2(k,iut),0.0d0,0.0d0)
                end if
              end do
            end if
          end if
          if (dn_head(jb)) then
            idt = id+1
            if (dh_internal(jb)) then
              do k=kt,kb(idt)
                do jc=1,nac
                  c1s(k,idt,cn(jc)) = c1s(k,dhs(jb),cn(jc))
                  c1(k,idt,cn(jc))  = c1s(k,dhs(jb),cn(jc))
                  c2(k,idt,cn(jc))  = c1s(k,dhs(jb),cn(jc))
                end do
                t1(k,idt)  = t2(k,dhs(jb))
                t2(k,idt)  = t2(k,dhs(jb))
                rho(k,idt) = rho(k,dhs(jb))
              end do
            else if (dh_external(jb)) then
              do k=kt,kb(idt)
                do jc=1,nac
                  c1s(k,idt,cn(jc)) = cdh(k,cn(jc),jb)
                  c1(k,idt,cn(jc))  = cdh(k,cn(jc),jb)
                  c2(k,idt,cn(jc))  = cdh(k,cn(jc),jb)
                end do
                t1(k,idt)  = tdh(k,jb)
                t2(k,idt)  = tdh(k,jb)
                if (constituents) then
                  rho(k,idt) = density (t2(k,idt),ss(k,idt),tds(k,idt))
                else
                  rho(k,idt) = density (t2(k,idt),0.0d0,0.0d0)
                end if
              end do
            end if
          end if

!***********************************************************************
!*                    task 2.2.2: momentum terms                      **
!***********************************************************************

!$message:'        momentum terms'
!******** densities

          do i=iu,id
            do k=kt,kb(i)
              if (constituents) then
              !<mmueller>
                 tds(k,i) = c2(k,i,5)+c2(k,i,6)+c2(k,i,8)+ c2(k,i,9)+ &
                 &          c2(k,i,10)+c2(k,i,11)+ c2(k,i,14)*3.6642022795580753 + &
                 !tic as CO2 for density
                 &          c2(k,i,17)+c1(k,i,18)+ c1(k,i,19)+ &
                 &          c2(k,i,20)+c2(k,i,22)+c2(k,i,23)+c2(k,i,24)+  &
                 &          c2(k,i,25)+c2(k,i,26)+c2(k,i,27)+c2(k,i,28)+ &
                 &          c2(k,i,29)+c2(k,i,30)+c2(k,i,31)+ c2(k,i,32)+ &
                 &          c2(k,i,33)+c2(k,i,36)+c2(k,i,37)+c2(k,i,38) + &
                 &          c2(k,i,39)
                 do i_add = ncp_fixed + 1, ncp - ncp_additional_minerals
                   tds(k,i) = tds(k,i) + c2(k, i, i_add)
                 enddo
              !</mmueller>
                rho(k,i) = density (t2(k,i),ss(k,i),tds(k,i))

              else
                rho(k,i) = density (t2(k,i),0.0d0,0.0d0)
              end if
            end do
          end do


!******** density pressures

          do i=iut,idt
            p(kt,i) = rho(kt,i)*g*h(kt)
            do k=kt+1,kb(i)
              p(k,i) = p(k-1,i)+rho(k,i)*g*h(k)
            end do
          end do

!******** horizontal density gradients

          do i=iut,idt-1
            hdg(kt,i) = (dlxrho(i)*0.5d0)*(bkt(i)+bkt(i+1))*((hkt2(i+1)    &
     &                  *p(kt,i+1))-(hkt2(i)*p(kt,i)))
            do k=kt+1,kbmin(i)
              hdg(k,i) = (dlxrho(i)*bhr(k,i))*((p(k-1,i+1)-p(k-1,i))    &
     &                   +(p(k,i+1)-p(k,i)))
            end do
          end do

!******** wind drag coefficient and surface shear stress

          cz = 0.0d0
          if (wind.ge.1.0d0)  cz = 0.0005*sqrt(wind)
          if (wind.ge.15.0) cz = 0.0026
          ssc = rhoa*cz*wind**2/rhow

!******** vertical eddy viscosities

          do i=iut,idt-1
            fetch  = fetchd(i,jb)
            ssccos = ssc*cos(phi-phi0(i))*icesw(i)
            sscsin = ssc*abs(sin(phi-phi0(i)))*icesw(i)
            if (cos(phi-phi0(i)).lt.0.0) fetch = fetchu(i,jb)
            wwt = 0.0d0
            if (wind.ne.0.0) then
              wwt = 6.95e-2*(fetch**0.233)*abs(wind)**0.5d034
            end if
            st(kt,i) = ssccos*br(kti(i),i)
            if (.not.one_layer(i)) then
              dfc        = (-8.0*3.14159*3.14159)/(g*wwt*wwt+nonzero)
              expdf      = exp(max(dfc*depthb(kt),-30.0))
              st(kt+1,i) = ssccos*expdf*br(kt+1,i)
              shears     = ((u(kt+1,i)-u(kt,i))/((avhkt(i)+avhkt(i+1))    &
     &                     *0.5d0))**2
              az0        = 0.4*hmax*hmax*sqrt(shears+(sscsin            &
     &                     *expdf/az(kt,i))**2)+azmin
              riaz0      = log(az0/azmax)/1.5
              buoy       = (rho(kt+1,i)-rho(kt,i)+rho(kt+1,i+1)            &
     &                     -rho(kt,i+1))/(2.0*avhkt(i))
              ri         = g*buoy/(rhow*shears+nonzero)
              riaz1      = max(ri,riaz0)
              riaz1      = min(riaz1,10.0)
              expraz     = exp(-1.5*riaz1)
              az(kt,i)   = max(azmin,az0*expraz+azmin*(1.0d0-expraz))
              dz(kt,i)   = max(dzmin,frazdz*(az0*expraz+dzmin            &
     &                     *(1.0d0-expraz)))
              kbt        = kbmin(i)
              do k=kt+2,kbt
                expdf     = exp(max(dfc*depthb(k-1),-30.0))
                st(k,i)   = ssccos*expdf*br(k,i)
                shears    = ((u(k,i)-u(k-1,i))/avh(k-1))**2
                az0       = 0.4*hmax*hmax*sqrt(shears+(sscsin            &
     &                      *expdf/az(k-1,i))**2)+azmin
                riaz0     = log(az0/azmax)/1.5
                buoy      = (rho(k,i)-rho(k-1,i)+rho(k,i+1)                &
     &                      -rho(k-1,i+1))/(2.0*avh(k-1))
                ri        = g*buoy/(rhow*shears+nonzero)
                riaz1     = max(ri,riaz0)
                riaz1     = min(riaz1,10.0)
                expraz    = exp(-1.5*riaz1)
                az(k-1,i) = max(azmin,az0*expraz+azmin*(1.0d0-expraz))
                dz(k-1,i) = max(dzmin,frazdz*(az0*expraz+dzmin            &
     &                      *(1.0d0-expraz)))
              end do
              sb(kbt,i) = ssccos*expdf*(br(kbt-1,i)+br(kbt,i))*0.5d0
            end if
          end do

!******** average eddy diffusivities

          do k=kt,kb(idt)-1
            dz(k,idt) = dz(k,idt-1)
          end do
          do i=iut,idt-1
            do k=kt,kb(i)-1
              if (k.ge.kbmin(i)) then
                if (kb(i-1).ge.kb(i).and.i.ne.iut) then
                  dz(k,i) = dz(k,i-1)
                else
                  dz(k,i) = dzmin
                end if
              else
                dz(k,i) = (dz(k,i)+dz(k+1,i))*0.5d0
              end if
            end do
          end do

!******** density inversions

          do i=iut,idt
            do k=kt,kb(i)-1
              dzq(k,i) = dz(k,i)
              if (rho(k,i).gt.rho(k+1,i)) dz(k,i) = dzmax
            end do
          end do

!******** shear stresses

          gc2 = g/(chezy*chezy)
          do i=iut,idt-1
            kbt = kbmin(i)
            if (.not.one_layer(i)) then
              st(kt+1,i) = st(kt+1,i)+az(kt,i)*(br(kt,i)+br(kt+1,i))    &
     &                     *0.5d0*(u(kt,i)-u(kt+1,i))/((avhkt(i)            &
     &                     +avhkt(i+1))*0.5d0)
            end if
            do k=kt+2,kbt
              st(k,i) = st(k,i)+az(k-1,i)*(br(k-1,i)+br(k,i))*0.5d0        &
     &                  *(u(k-1,i)-u(k,i))/avh(k-1)
            end do
            do k=kt,kbt-1
              sb(k,i) = st(k+1,i)+gc2*(br(k,i)-br(k+1,i))*u(k,i)        &
     &                  *abs(u(k,i))
            end do
            sb(kbt,i) = sb(kbt,i)+gc2*br(kbt,i)*u(kbt,i)*abs(u(kbt,i))
          end do

!******** horizontal momentum

          do i=iu,id-1
            udr        = (1.0d0+sign(1.0d0,(u(kt,i)+u(kt,i+1))*0.5d0))*0.5d0
            udl        = (1.0d0+sign(1.0d0,(u(kt,i)+u(kt,i-1))*0.5d0))*0.5d0
            admx(kt,i) = (bhkt2(i+1)*(u(kt,i+1)+u(kt,i))*0.5d0            &
     &                   *(udr*u(kt,i)+(1.0d0-udr)*u(kt,i+1))-bhkt2(i)    &
     &                   *(u(kt,i)+u(kt,i-1))*0.5d0*(udl*u(kt,i-1)        &
     &                   +(1.0d0-udl)*u(kt,i)))/dlxr(i)
            dm(kt,i)   = ax*(bhkt2(i+1)*(u(kt,i+1)-u(kt,i))/dlx(i+1)    &
     &                   -bhkt2(i)*(u(kt,i)-u(kt,i-1))/dlx(i))/dlxr(i)
            do k=kt+1,kbmin(i)
              udr       = (1.0d0+sign(1.0d0,(u(k,i)+u(k,i+1))*0.5d0))*0.5d0
              udl       = (1.0d0+sign(1.0d0,(u(k,i)+u(k,i-1))*0.5d0))*0.5d0
              admx(k,i) = (bh(k,i+1)*(u(k,i+1)+u(k,i))*0.5d0*(udr*u(k,i)    &
     &                    +(1.0d0-udr)*u(k,i+1))-bh(k,i)*(u(k,i)            &
     &                    +u(k,i-1))*0.5d0*(udl*u(k,i-1)+(1.0d0-udl)        &
     &                    *u(k,i)))/dlxr(i)
              dm(k,i)   = ax*(bh(k,i+1)*(u(k,i+1)-u(k,i))/dlx(i+1)        &
     &                    -bh(k,i)*(u(k,i)-u(k,i-1))/dlx(i))/dlxr(i)
            end do
          end do

!******** vertical momentum

          do i=iu,id-1
            do k=kt,kb(i)-1
              ud        = (1.0d0+sign(1.0d0,(w(k,i+1)+w(k,i))*0.5d0))*0.5d0
              admz(k,i) = (br(k,i)+br(k+1,i))*0.5d0*(w(k,i+1)+w(k,i))        &
     &                    *0.5d0*(ud*u(k,i)+(1.0d0-ud)*u(k+1,i))
            end do
          end do

!***********************************************************************
!*                task 2.2.3: water surface elevation                 **
!***********************************************************************

!$message:'        water surface elevation'
!******** tridiagonal coefficients

          do i=iu,id-1
            bhrho(i) = bhkt2(i+1)/rho(kt,i+1)+bhkt2(i)/rho(kt,i)
            do k=kt+1,kbmin(i)
              bhrho(i) = bhrho(i)+(bh(k,i+1)/rho(k,i+1)+bh(k,i)            &
     &                   /rho(k,i))
            end do
            d(i) = u(kt,i)*bhrkt2(i)-u(kt,i-1)*bhrkt2(i-1)-qss(kt,i)
            !write (*,*) 'f(i) vorher', f(i)
            !write(*,*) sb(kt,i), st(kt,i), admx(kt,i), dm(kt,i), hdg(kt,i)
            f(i) = -sb(kt,i)+st(kt,i)-admx(kt,i)+dm(kt,i)-hdg(kt,i)
            !write (*,*) 'f(i) nachher', f(i)
            do k=kt+1,kb(i)
              d(i) = d(i)+(u(k,i)*bhr(k,i)-u(k,i-1)*bhr(k,i-1)-qss(k,i))
              f(i) = f(i)+(-sb(k,i)+st(k,i)-admx(k,i)+dm(k,i)-hdg(k,i))
            end do
          end do
          d(iu) = u(kt,iu)*bhrkt2(iu)-qss(kt,iu)
          do k=kt+1,kb(iu)
            d(iu) = d(iu)+(u(k,iu)*bhr(k,iu)-qss(k,iu))
          end do
!******** boundary tridiagonal coefficients

          if (up_flow(jb)) d(iu) = d(iu)-qin(jb)
          if (dn_flow(jb)) then
            d(id) = -u(kt,id-1)*bhrkt2(id-1)-qss(kt,id)
            do k=kt+1,kb(id)
              d(id) = d(id)-(u(k,id-1)*bhr(k,id-1)+qss(k,id))
            end do
            do k=kt,kb(id)
              d(id) = d(id)+qout(k,jb)
            end do
          end if
          if (up_head(jb)) then
            bhrho(iu-1) = bhkt2(iu)/rho(kt,iu)+bhkt2(iu-1)/rho(kt,iu-1)
            do k=kt+1,kb(iu-1)
              bhrho(iu-1) = bhrho(iu-1)+(bh(k,iu)/rho(k,iu)+bh(k,iu-1)    &
     &                      /rho(k,iu-1))
            end do
            d(iu)   = d(iu)-u(kt,iu-1)*bhrkt2(iu-1)
            f(iu-1) = -sb(kt,iu-1)+st(kt,iu-1)-hdg(kt,iu-1)
            do k=kt+1,kb(iu)
              d(iu)   = d(iu)-u(k,iu-1)*bhr(k,iu-1)
              f(iu-1) = f(iu-1)-(sb(k,iu-1)-st(k,iu-1)+hdg(k,iu-1))
            end do
          end if
          if (dn_head(jb)) then
            bhrho(id) = bhkt2(id+1)/rho(kt,id+1)+bhkt2(id)/rho(kt,id)
            do k=kt+1,kb(id+1)
              bhrho(id) = bhrho(id)+(bh(k,id+1)/rho(k,id+1)+bh(k,id)    &
     &                    /rho(k,id))
            end do
            d(id) = u(kt,id)*bhrkt2(id)-u(kt,id-1)*bhrkt2(id-1)            &
     &                -qss(kt,id)
            f(id) = -sb(kt,id)+st(kt,id)-hdg(kt,id)
            do k=kt+1,kb(id)
              d(id) = d(id)+(u(k,id)*bhr(k,id)-u(k,id-1)*bhr(k,id-1)    &
     &                -qss(k,id))
              f(id) = f(id)+(-sb(k,id)+st(k,id)-hdg(k,id))
            end do
          end if
        end do
        do jb=1,nbp
          iu = cus(jb)
          id = ds(jb)


!******** boundary surface elevations

          if (uh_internal(jb)) z(iu-1) = z(uhs(jb))
          if (uh_external(jb)) z(iu-1) = el(kt)-eluh(jb)
          if (dh_internal(jb)) z(id+1) = z(dhs(jb))
          if (dh_external(jb)) z(id+1) = el(kt)-eldh(jb)



         do i=iu,id
            a(i) = -rho(kt,i-1)*g*dlt**2*bhrho(i-1)*0.5d0/dlxr(i-1)
            c(i) = -rho(kt,i+1)*g*dlt**2*bhrho(i)*0.5d0/dlxr(i)
            v(i) = rho(kt,i)*g*dlt**2*(bhrho(i)*0.5d0/dlxr(i)                &
     &             +bhrho(i-1)*0.5d0/dlxr(i-1))+dlx(i)*b(kti(i),i)
            d(i) = dlt*(d(i)+dlt*(f(i)-f(i-1)))+dlx(i)*b(kti(i),i)*z(i)




          end do


          if (up_head(jb)) d(iu) = d(iu)-a(iu)*z(iu-1)
          if (dn_head(jb)) d(id) = d(id)-c(id)*z(id+1)



!******** implicit water surface elevation

          bta(iu) = v(iu)
          gma(iu) = d(iu)/bta(iu)


          do i=iu+1,id
            bta(i) = v(i)-a(i)*c(i-1)/bta(i-1)
            gma(i) = (d(i)-a(i)*gma(i-1))/bta(i)
          end do
          z(id) = gma(id)

          do k=1,id-iu
            i    = id-k
            z(i) = gma(i)-c(i)*z(i+1)/bta(i)
          end do
          if (up_flow(jb)) z(iu-1) = z(iu)
          if (dn_flow(jb)) z(id+1) = z(id)






!******** updated surface layer and related variables

          do i=iu-1,id+1
            if (el(kt)-z(i).gt.el(kti(i))) then
              do while (el(kt)-z(i).gt.el(kti(i)).and.kti(i).ne.2)
                z(i)   = el(kt)-el(kti(i))-(el(kt)-el(kti(i))-z(i))        &
     &                   *(b(kti(i),i)/b(kti(i)-1,i))
                kti(i) = kti(i)-1
              end do
            else if (el(kt)-z(i).lt.el(kti(i)+1)) then
              do while (el(kt)-z(i).lt.el(kti(i)+1))
                z(i)   = el(kt)-el(kti(i)+1)-(el(kt)-el(kti(i)+1)        &
     &                   -z(i))*(b(kti(i),i)/b(kti(i)+1,i))
                kti(i) = kti(i)+1
              end do
            end if

            hkt1(i)  = h(kt)-z(i)
            avhkt(i) = (hkt1(i)+h(kt+1))*0.5d0
            bhkt1(i) = b(kti(i),i)*(el(kt)-z(i)-el(kti(i)+1))
            do k=kti(i)+1,kt
              bhkt1(i) = bhkt1(i)+bh(k,i)
            end do
            bkt(i) = bhkt1(i)/hkt1(i)
          end do





          do i=iu-1,id
            bhrkt1(i) = (bhkt1(i)+bhkt1(i+1))*0.5d0
          end do
          bhrkt1(id+1) = bhkt1(id+1)
          dlvol(jb)    = 0.0d0
          do i=iu,id
            dlvol(jb) = dlvol(jb)+(bhkt1(i)-bhkt2(i))*dlx(i)
          end do



!******** layer bottom and middle depths

          depthb(kt)   = hkt1(i)
          depthm(kt)   = hkt1(i)*0.5d0
          depthb(kt+1) = depthb(kt)+h(kt+1)
          depthm(kt+1) = depthm(kt)+h(kt+1)*0.5d0
          do k=kt+2,kmp-1
            depthb(k) = depthb(k-1)+h(k)
            depthm(k) = depthm(k-1)+(h(k-1)+h(k))*0.5d0
          end do

!***********************************************************************
!*                task 2.2.4: longitudinal velocities                 **
!***********************************************************************

!$message:'        longitudinal velocities'
          iut = iu
          idt = id
          if (up_head(jb)) iut = iu-1
          if (dn_head(jb)) idt = id+1

!******** pressures

          do i=iut,idt
            p(kt,i) = rho(kt,i)*g*hkt1(i)
            do k=kt+1,kb(i)
              p(k,i) = p(k-1,i)+rho(k,i)*g*h(k)
            end do
          end do

!******** horizontal pressure gradients

          do i=iut,idt-1
            hpg(kt,i) = (dlxrho(i)*0.5d0)*(bkt(i)+bkt(i+1))*((hkt1(i+1)    &
     &                  *p(kt,i+1))-(hkt1(i)*p(kt,i)))
            do k=kt+1,kbmin(i)
              hpg(k,i) = dlxrho(i)*bhr(k,i)*((p(k-1,i+1)-p(k-1,i))        &
     &                   +(p(k,i+1)-p(k,i)))
            end do
          end do

!******** boundary horizontal velocities

          if (up_flow(jb)) then
            do k=kt,kb(iu)
              qinf(k,jb) = 0.0d0
            end do
            if (place_qin) then

!************ inflow layer

              k     = kt
              rhoin = density (tin(jb),cin(2,jb),cin(4,jb))
              do while (rhoin.gt.rho(k,iu).and.k.lt.kb(iu))
                k = k+1
              end do
              ktqin(jb) = k
              kbqin(jb) = k

!************ layer inflows

              vqin  = qin(jb)*dlt
              vqini = vqin
              qinfr = 1.0d0
              incr  = -1
              do while (qinfr.gt.0.0)
                vol  = bh(k,iu)*dlx(iu)
                if (k.eq.kt) vol = bhkt1(iu)*dlx(iu)
                if (k.le.kb(iu)) then
                  if (vqin.gt.0.5d0*vol) then
                    qinf(k,jb) = 0.5d0*vol/vqini
                    qinfr      = qinfr-qinf(k,jb)
                    vqin       = vqin-qinf(k,jb)*vqini
                    if (k.eq.kt) then
                      k    = kbqin(jb)
                      incr = 1
                    end if
                  else
                    qinf(k,jb) = qinfr
                    qinfr      = 0.0d0
                  end if
                  if (incr.lt.0) ktqin(jb) = k
                  if (incr.gt.0) kbqin(jb) = min(kb(iu),k)
                  k = k+incr
                else
                  qinf(kt,jb) = qinfr
                  qinfr       = 0.0d0
                end if
              end do
            else
              bhsum = bhkt1(iu)
              do k=kt+1,kb(iu)
                bhsum = bhsum+bh(k,iu)
              end do
              qinf(kt,jb) = bhkt1(iu)/bhsum
              do k=kt+1,kb(iu)
                qinf(k,jb) = bh(k,iu)/bhsum
              end do
              ktqin(jb) = kt
              kbqin(jb) = kb(iu)
            end if
            u(kt,iu-1) = qinf(kt,jb)*qin(jb)/bhrkt1(iu-1)
            do k=kt+1,kb(iu)
              u(k,iu-1) = qinf(k,jb)*qin(jb)/bhr(k,iu-1)
            end do
          end if
          if (dn_flow(jb)) then
            do k=kt,kb(id)
              bhrt = bhr(k,id)
              if (k.eq.kt) bhrt = bhrkt1(id)
              u(k,id) = qout(k,jb)/bhrt
            end do
          end if
          if (up_head(jb)) then
            u(kt,iu-1) = (bhrkt2(iu-1)*u(kt,iu-1)+dlt*(-sb(kt,iu-1)        &
     &                   +st(kt,iu-1)-hpg(kt,iu-1)))/bhrkt1(iu-1)
            do k=kt+1,kb(iu-1)
              u(k,iu-1) = (bhr(k,iu-1)*u(k,iu-1)+dlt*(-sb(k,iu-1)        &
     &                    +st(k,iu-1)-hpg(k,iu-1)))/bhr(k,iu-1)
            end do


          end if
          if (dn_head(jb)) then
            u(kt,id) = (bhrkt2(id)*u(kt,id)+dlt*(-sb(kt,id)+st(kt,id)    &
     &                 -hpg(kt,id)))/bhrkt1(id)
            do k=kt+1,kb(id+1)
              u(k,id) = (bhr(k,id)*u(k,id)+dlt*(-sb(k,id)+st(k,id)        &
     &                  -hpg(k,id)))/bhr(k,id)
            end do
          end if


!******** horizontal velocities

          do i=iu,id-1
            u(kt,i) = (bhrkt2(i)*u(kt,i)+dlt*(-sb(kt,i)+st(kt,i)        &
     &                -admz(kt,i)+dm(kt,i)-admx(kt,i)-hpg(kt,i)))        &
     &                /bhrkt1(i)
            do k=kt+1,kbmin(i)
              u(k,i) = u(k,i)+dlt/bhr(k,i)*(-sb(k,i)+st(k,i)-admz(k,i)    &
     &                 +admz(k-1,i)-admx(k,i)+dm(k,i)-hpg(k,i))
            end do
          end do

!******** corrected horizontal velocities

          if (up_head(jb)) then
            is   = id
            ie   = iu-1
            incr = -1
            q(is) = u(kt,is)*bhrkt1(is)
            do k=kt+1,kb(id)
              q(is) = q(is)+u(k,is)*bhr(k,is)
            end do
          else
            is   = iu-1
            ie   = id
            incr = 1
            if (dn_flow(jb)) ie = id-1
            q(is) = u(kt,is)*bhrkt1(is)
            do k=kt+1,kb(iu)
              q(is) = q(is)+u(k,is)*bhr(k,is)
            end do
          end if
          qc(is) = q(is)
          do i=is+incr,ie,incr
            qssum(i) = qss(kt,i)
            do k=kt+1,kb(i)
              qssum(i) = qssum(i)+qss(k,i)
            end do
            bhrsum = bhrkt1(i)
            q(i)   = u(kt,i)*bhrkt1(i)
            do k=kt+1,kbmin(i)
              bhrsum = bhrsum+bhr(k,i)
              q(i)   = q(i)+u(k,i)*bhr(k,i)
            end do
            if (up_head(jb)) then
              qc(i) = qc(i+1)+(bhkt1(i+1)-bhkt2(i+1))*dlx(i+1)/dlt        &
     &                -qssum(i+1)
            else
              qc(i) = qc(i-1)-(bhkt1(i)-bhkt2(i))*dlx(i)/dlt+qssum(i)
            end if
            do k=kt,kbmin(i)
              u(k,i) = u(k,i)+(qc(i)-q(i))/bhrsum
            end do
          end do

!******** head boundary flows

          if (up_head(jb)) then
            quh1(kt,jb) = u(kt,iu-1)*bhrkt1(iu-1)
            do k=kt+1,kb(iu-1)
              quh1(k,jb) = u(k,iu-1)*bhr(k,iu-1)
            end do
          end if
          if (dn_head(jb)) then
            qdh1(kt,jb) = u(kt,id)*bhrkt1(id)
            do k=kt+1,kb(id+1)
              qdh1(k,jb) = u(k,id)*bhr(k,id)
            end do
          end if

!***********************************************************************
!*                  task 2.2.5: vertical velocities                   **
!***********************************************************************

!$message:'        vertical velocities'

          do i=iu,id
            do k=kb(i)-1,kt,-1
              wt1    = w(k+1,i)*bb(k+1,i)
              wt2    = (bhr(k+1,i)*u(k+1,i)-bhr(k+1,i-1)*u(k+1,i-1)        &
     &                 -qss(k+1,i))/dlx(i)
              w(k,i) = (wt1+wt2)/bb(k,i)
            end do
          end do
        end do

!***********************************************************************
!*                     task 2.2.6: autostepping                       **
!***********************************************************************
!write(*,*) 'auto begin'
!$message:'        autostepping'
        do jb=1,nbp
          do i=cus(jb),ds(jb)
            if (hkt1(i).lt.0.0) then
              write (err,6020) jday,i,z(i),hseg(kt,i)-z(i)
              write(*,*) '***severe*** computational error - see "w2.err"'
              write (*,*) jday,i,z(i),hseg(kt,i)-z(i)
              error = 0
              return
            end if
            tau1   = 2.0*max(ax,dxi)/(dlx(i)*dlx(i))
            tau2   = 2.0*az(kt,i)/(hkt1(i)*hkt1(i))
            celrty = sqrt((abs(rho(kb(i),i)-rho(kt,i)))/1000.0*g        &
     &               *depthb(kb(i))*0.5d0)
            qtot   = (abs(u(kt,i))*bhrkt1(i)+abs(u(kt,i-1))*bhrkt1(i-1)    &
     &               +abs(w(kt,i))*bb(kt,i)*dlx(i)+dlx(i)*abs(bhkt2(i)    &
     &               -bhkt1(i))/dlt+abs(qss(kt,i)))*0.5d0
            dltcal = 1.0d0/((qtot/bhkt1(i)+celrty)/dlx(i)+tau1+tau2)
            if (dltcal.lt.curmax) then
              kloc   = kt
              iloc   = i
              curmax = int(dltcal)
              if (dltf(dltdp)*curmax.lt.mindlt) then
                kmin = kt
                imin = i
              end if
            end if
            do k=kt+1,kb(i)
              tau2   = 2.0*az(k,i)/(h(k)*h(k))
              qtot   = (abs(u(k,i))*bhr(k,i)+abs(u(k,i-1))*bhr(k,i-1)    &
     &                 +(abs(w(k,i))*bb(k,i)+abs(w(k-1,i))*bb(k-1,i))    &
     &                 *dlx(i)+abs(qss(k,i)))*0.5d0
              dltcal = 1.0d0/((qtot/bh(k,i)+celrty)/dlx(i)+tau1+tau2)
              if (dltcal.lt.curmax) then
                kloc   = k
                iloc   = i
                curmax = int(dltcal)
                if (dltf(dltdp)*curmax.lt.mindlt) then
                  kmin = k
                  imin = i
                end if
              end if
            end do
          end do
        end do

!****** limiting location

        if (limiting_dlt) then
          ndlt(kloc,iloc) = ndlt(kloc,iloc)+1
          do i=iu,id
            do k=kt,kb(i)
              if (ndlt(kloc,iloc).gt.limdlt) then
                klim   = kloc
                ilim   = iloc
                limdlt = ndlt(kloc,iloc)
              end if
            end do
          end do
        end if

!****** restore timestep dependent variables and restart calculations

        if (curmax.lt.dlt.and.dlt.gt.dltmin) then
          dlt = int(dltf(dltdp)*curmax)
          if (dlt.le.dltmin) then
            warning_open = .true.
            write (wrn,6000) jday,dlt
            dlt = dltmin
          end if
          curmax = dltmax(dltdp)/dltf(dltdp)
          if (dlt.lt.mindlt) then
            mindlt = dlt
            jdmin  = jday+dlt/86400.0
          end if
          do jb=1,nbp
            do i=cus(jb)-1,ds(jb)+1
              z(i)   = sz(i)
              kti(i) = skti(i)
              do k=kt,max(kb(iu),kb(i))
                u(k,i)   = su(k,i)
                w(k,i)   = sw(k,i)
                az(k,i)  = saz(k,i)
                qss(k,i) = 0.0d0
              end do
            end do
          end do
          nv = nv+1
          go to 10010
        end if

!***********************************************************************
!*         task 2.3: temporal balance terms and temperatures          **
!***********************************************************************
!write(*,*) 'balance'


!$message:'    temperatures'
!$message:'      sources/sinks'
        if (.not.no_heat) then
          call heat_exchange ()
          rs = sro*rhowcp
          if (term_by_term) call radiation ()
        end if
       !write(*,*) 'source'
        do jb=1,nbp
          iu = cus(jb)
          id = ds(jb)
!write(*,*) 'loop'
!******** heat exchange

          if (.not.no_heat) then
            do i=iu,id

!************ surface

              if (.not.ice(i)) then

!************** surface exchange


                if (term_by_term) then
                  call surface_terms (t2(kt,i))
                  rn(i) = rsn+ran-rb-re-rc
                  tflux = rn(i)/rhowcp*b(kti(i),i)*dlx(i)
                else
                  tflux = cshe*(et-t2(kt,i))*b(kti(i),i)*dlx(i)
                end if
                tss(kt,i) = tss(kt,i)+tflux
                tsss(jb)  = tsss(jb)+tflux*dlt

!************** solar radiation

                if (constituents) then
                  gamma = exh2o+exss*ss(kt,i)+exom*(algae(kt,i)            &
     &                    +lpom(kt,i))
                else
                  gamma = exh2o
                end if
                !<mmueller>
                if (beta .le. 1e-6) then
                    beta = 0.27*log(gamma) +0.61
                end if
                !</mmueller>
                tflux     = (1.0d0-beta)*sron*exp(-gamma*depthb(kt))        &
     &                      *b(kti(i),i)*dlx(i)
                tss(kt,i) = tss(kt,i)-tflux
                tsss(jb)  = tsss(jb)-tflux*dlt
                do k=kt+1,kb(i)
                  if (constituents) then
                    gamma = exh2o+exss*ss(k,i)+exom*(algae(k,i)            &
     &                      +lpom(k,i))
                  else
                    gamma = exh2o
                  end if
                  !<mmueller>
                  if (beta .le. 1e-6) then
                        beta = 0.27*log(gamma) +0.61
                  end if
                  !</mmueller>
                  tflux    = (1.0d0-beta)*sron*(exp(-gamma*depthb(k))        &
     &                       -exp(-gamma*depthb(k+1)))*b(k,i)*dlx(i)

                  tss(k,i) = tss(k,i)+tflux
                  tsss(jb) = tsss(jb)+tflux*dlt

                end do
              end if

!************ bottom

              tflux     = cbhe*(tsed-t2(kt,i))*(b(kti(i),i)                &
     &                    -b(kt+1,i))*dlx(i)
              tss(kt,i) = tss(kt,i)+tflux
              tssb(jb)  = tssb(jb)+tflux*dlt
              do k=kt+1,kb(i)-1
                tflux    = cbhe*(tsed-t2(k,i))*(b(k,i)                    &
     &                     -b(k+1,i))*dlx(i)
                tss(k,i) = tss(k,i)+tflux
                tssb(jb) = tssb(jb)+tflux*dlt
              end do
              tflux        = cbhe*(tsed-t2(kb(i),i))                    &
     &                       *(b(kb(i),i))*dlx(i)
              tss(kb(i),i) = tss(kb(i),i)+tflux
              tssb(jb)     = tssb(jb)+tflux*dlt
            end do

!********** ice cover
!write(*,*) 'ice'

            if (ice_calc) then
              hia = 0.2367*cshe/5.65e-8
              do i=iu,id
                allow_ice(i) = .true.
                do k=kt,kb(i)
                  if (t2(k,i).gt.icet2) allow_ice(i) = .false.
                end do
              end do
              ice_in(jb) = .true.
              do i=iu,id
                if (iceth(i).lt.icemin) ice_in(jb) = .false.
              end do
              do i=iu,id
                if (detailed_ice) then
                  if (t2(kt,i).lt.0.0) then
                    if (.not.ice(i)) then
                      iceth2 = -t2(kt,i)*rho(kt,i)*cp*hkt2(i)/rhorl1
                      if (iceth2.lt.ice_tol) then
                        iceth2 = 0.0d0
                      else
                        heat       = t2(kt,i)*rho(kt,i)*cp*hkt2(i)        &
     &                               *b(kti(i),i)/(4.186e6*dlt)*dlx(i)
                        tss(kt,i)  = tss(kt,i)-heat
                        tssice(jb) = tssice(jb)-heat*dlt
                      end if
                    end if
                  end if

!**************** ice balance

                  if (ice(i)) then
                    tice = tair
                    del  = 2.0
                    j    = 1
                    do while (del.gt.1.0d0.and.j.lt.500)
                      call surface_terms (tice)
                      rn(i) = rs*(1.0d0-albedo)*betai+ran-rb-re-rc


!******************** heat balance

                      del = rn(i)+rk1*(rimt-tice)/iceth(i)
                      if (abs(del).gt.1.0d0) then
                          tice = tice + del /500.0
                      end if
                      j = j+1
                    end do

!****************** solar radiation attenuation

                    heat       = dlx(i)*sron*(1.0d0-albedo)*(1.0d0-betai)    &
     &                           *exp(-gammai*iceth(i))*b(kti(i),i)
                    tss(kt,i)  = tss(kt,i)+heat
                    tssice(jb) = tssice(jb)+heat*dlt
                    if (tice.gt.0.0) then
                      !hice   = rhoicp*0.5d0*tice*0.5d0*iceth(i)            &
     &                !         *b(kti(i),i)/4.186e6/dlt
                      !icethu = -dlt*hice/b(kti(i),i)*4.186e6/rhorl1
                      ! TODO: Why do we need to divide by 10 to get a
                      ! reasoble melting. Otherwise it is way too fast.
                      icethu = -(cpi * 0.5d0 * tice * iceth(i)) / latf / 10
                      !write(debug,*) icethu
                      ! WHY ???
                      !if (iceth(i) > 0.1) then
                      !    icethu = icethu / 100.0
                      !end if
                      tice = 0.0d0
                    end if

!****************** ice growth

                    if (tice.lt.0.0) iceth1 = dlt*(rk1*(rimt-tice)        &
     &                                        /iceth(i))/rhorl1
!****************** ice melt

                    if (t2(kt,i).gt.0.0) then
                      iceth2     = -dlt*hwi*(t2(kt,i)-rimt)/rhorl1
                      heat       = 2.392e-7*hwi*(rimt-t2(kt,i))            &
     &                             *b(kti(i),i)*dlx(i)
                      tss(kt,i)  = tss(kt,i)+heat
                      tssice(jb) = tssice(jb)+heat*dlt
                    end if
                  end if

!**************** ice thickness

                  iceth(i) = iceth(i)+icethu+iceth1+iceth2
                  if (iceth(i).lt.ice_tol) iceth(i) = 0.0d0
                  if (winter.and.(.not.ice_in(jb))) then
                    if (.not.allow_ice(i)) iceth(i) = 0.0d0
                  end if
                  ice(i)   = iceth(i).gt.0.0
                  icesw(i) = 1.0d0
                  if (ice(i)) icesw(i) = 0.0d0
                  icethu = 0.0d0
                  iceth1 = 0.0d0
                  iceth2 = 0.0d0
                  if (iceth(i).lt.ice_tol.and.iceth(i).gt.0.0) then
                    iceth(i) = ice_tol
                  end if
                else
                  hia      = 0.2367*cshe/5.65e-8
                  iceth(i) = iceth(i)+dlt*((rimt-et)/(iceth(i)/rk1+1.0d0    &
     &                       /hia)-(t2(kt,i)-rimt))/rhorl1
                  iceth(i) = max(iceth(i),0.0)
                  ice(i)   = iceth(i).gt.0.0
                  icesw(i) = 1.0d0
                  if (ice(i)) then
                    heat       = 2.392e-7*(rimt-t2(kt,i))*b(kti(i),i)    &
     &                           *dlx(i)
                    tss(kt,i)  = tss(kt,i)+heat
                    tssice(jb) = tssice(jb)+heat*dlt
                  end if
                end if
              end do
            end if
          end if
!write(*,*) 'heat'
!******** heat sources/sinks & total inflow/outflow

          if (evaporation) then
            do i=iu,id
              tss(kt,i) = tss(kt,i)-ev(i)*t2(kt,i)
              tssev(jb) = tssev(jb)-ev(i)*t2(kt,i)*dlt
              volev(jb) = volev(jb)-ev(i)*dlt
            end do
          end if
          if (precipitation) then
            do i=iu,id
              tss(kt,i) = tss(kt,i)+tpr(jb)*qpr(i)
              tsspr(jb) = tsspr(jb)+tpr(jb)*qpr(i)*dlt
              volpr(jb) = volpr(jb)+qpr(i)*dlt
            end do
          end if
          !write(*,*) 'after precip'
          if (tributaries) then
            do jt=1,ntr
              !write(*,*) 'jt', jt, ntr
              if (jb.eq.jbtr(jt)) then
                i = itr(jt)
                if (i.lt.cus(jb)) i = cus(jb)
                !write(*,*) 'voltr shape', shape(voltr)
                !write(*,*) 'jb', jb, 'nbp', nbp
                !write(*,*) 'voltr', voltr
                !write(*,*) 'kttr', kttr
                !write(*,*) 'kbtr', kbtr
                do k=kttr(jt),kbtr(jt)
                   !write(*,*) 'k', k
                   !write(*,*) 'qtr', qtr(jt)
                   !write(*,*) 'tsstr', tsstr
                  if (qtr(jt).lt.0.0) then
                    tss(k,i)  = tss(k,i)+t2(k,i)*qtr(jt)*qtrf(k,jt)
                    tsstr(jb) = tsstr(jb)+t2(k,i)*qtr(jt)*qtrf(k,jt)*dlt
                  else
                    tss(k,i)  = tss(k,i)+ttr(jt)*qtr(jt)*qtrf(k,jt)
                    tsstr(jb) = tsstr(jb)+ttr(jt)*qtr(jt)*qtrf(k,jt)*dlt
                  end if
                  voltr(jb) = voltr(jb)+qtr(jt)*qtrf(k,jt)*dlt
                  vol_trib_single(jt) = vol_trib_single(jt)+qtr(jt)*qtrf(k,jt)*dlt
                  !write(*,*) 'voltr', voltr
                  !write(*,*) shape(tsstr)
                  !write(*,*) 'tsstr', tsstr
                  !write(*,*) k
                end do
              end if
            end do
          end if
          !write(*,*) 'after tribs'
          if (dist_tribs(jb)) then
            do i=iu,id
              tss(kt,i) = tss(kt,i)+tdtr(jb)*qdt(i)
              tssdt(jb) = tssdt(jb)+tdtr(jb)*qdt(i)*dlt
              voldt(jb) = voldt(jb)+qdt(i)*dlt
            end do
          end if
          if (withdrawals) then
            do jw=1,nwd
              if (jb.eq.jbwd(jw)) then
                i         = max(cus(jbwd(jw)),iwd(jw))
                k         = max(kt,kwd(jw))
                tss(k,i)  = tss(k,i)-t2(k,i)*qwd(jw)
                tsswd(jb) = tsswd(jb)-t2(k,i)*qwd(jw)*dlt
                volwd(jb) = volwd(jb)-qwd(jw)*dlt
              end if
            end do
          end if
          if (up_flow(jb)) then
            volin(jb) = volin(jb)+qin(jb)*dlt
            do k=kt,kb(iu)
              tss(k,iu)  = tss(k,iu)+qinf(k,jb)*qin(jb)*tin(jb)
              tssin(jb)  = tssin(jb)+qinf(k,jb)*qin(jb)*tin(jb)*dlt
            end do
          end if
          if (dn_flow(jb)) then
            do k=kt,kb(id)
              tss(k,id)  = tss(k,id)-qout(k,jb)*t2(k,id)
              tssout(jb) = tssout(jb)-qout(k,jb)*t2(k,id)*dlt
              volout(jb) = volout(jb)-qout(k,jb)*dlt
            end do
          end if
          if (up_head(jb)) then
            iut = iu
            if (quh1(kt,jb).ge.0.0) iut = iu-1
            tssuh1(kt,jb) = t2(kt,iut)*quh1(kt,jb)
            tss(kt,iu)    = tss(kt,iu)+tssuh1(kt,jb)
            tssuh(jb)     = tssuh(jb)+tssuh1(kt,jb)*dlt
            voluh(jb)     = voluh(jb)+quh1(kt,jb)*dlt
            if (quh1(kt,jb) >= 0.0) then
                quh1_cum_plus(kt,jb) = quh1_cum_plus(kt,jb) + quh1(kt,jb)*dlt
            else
                quh1_cum_minus(kt,jb) = quh1_cum_minus(kt,jb) + quh1(kt,jb)*dlt
            end if
            do k=kt+1,kb(iu)
              iut = iu
              if (quh1(k,jb).ge.0.0) iut = iu-1
              tssuh1(k,jb) = t2(k,iut)*quh1(k,jb)
              tss(k,iu)    = tss(k,iu)+tssuh1(k,jb)
              tssuh(jb)    = tssuh(jb)+tssuh1(k,jb)*dlt
              voluh(jb)    = voluh(jb)+quh1(k,jb)*dlt
              if (quh1(k,jb) >= 0.0) then
                  quh1_cum_plus(k,jb) = quh1_cum_plus(k,jb) + quh1(k,jb)*dlt
              else
                  quh1_cum_minus(k,jb) = quh1_cum_minus(k,jb) + quh1(k,jb)*dlt
              end if
            end do
          end if
          if (uh_internal(jb)) then
            do k=kt,kb(iu-1)
              tss(k,uhs(jb))  = tss(k,uhs(jb))-tssuh2(k,jb)/dlt
              tssuh(jbuh(jb)) = tssuh(jbuh(jb))-tssuh2(k,jb)
              voluh(jbuh(jb)) = voluh(jbuh(jb))-quh2(k,jb)
              if (quh2(k,jb) >= 0.0) then
                  quh2_cum_plus(k,jb) = quh2_cum_plus(k,jb) + quh2(k,jb)*dlt
              else
                  quh2_cum_minus(k,jb) = quh2_cum_minus(k,jb) + quh2(k,jb)*dlt
              end if
            end do
          end if
          if (dn_head(jb)) then
            idt = id+1
            if (qdh1(kt,jb).ge.0.0) idt = id
            tssdh1(kt,jb) = t2(kt,idt)*qdh1(kt,jb)
            tss(kt,id)    = tss(kt,id)-tssdh1(kt,jb)
            tssdh(jb)     = tssdh(jb)-tssdh1(kt,jb)*dlt
            voldh(jb)     = voldh(jb)-qdh1(kt,jb)*dlt
            if (qdh1(kt,jb) >= 0.0) then
                qdh1_cum_plus(kt,jb) = qdh1_cum_plus(kt,jb) + qdh1(kt,jb)*dlt
            else
                qdh1_cum_minus(kt,jb) = qdh1_cum_minus(kt,jb) + qdh1(kt,jb)*dlt
            end if
            do k=kt+1,kb(id+1)
              idt = id+1
              if (qdh1(k,jb).ge.0.0) idt = id
              tssdh1(k,jb) = t2(k,idt)*qdh1(k,jb)
              tss(k,id)    = tss(k,id)-tssdh1(k,jb)
              tssdh(jb)    = tssdh(jb)-tssdh1(k,jb)*dlt
              voldh(jb)    = voldh(jb)-qdh1(k,jb)*dlt
              if (qdh1(k,jb) >= 0.0) then
                  qdh1_cum_plus(k,jb) = qdh1_cum_plus(k,jb) + qdh1(k,jb)*dlt
              else
                  qdh1_cum_minus(k,jb) = qdh1_cum_minus(k,jb) + qdh1(k,jb)*dlt
              end if
            end do
          end if
          if (dh_internal(jb)) then
            do k=kt,kb(id+1)
              tss(k,dhs(jb))  = tss(k,dhs(jb))+tssdh2(k,jb)/dlt
              tssdh(jbdh(jb)) = tssdh(jbdh(jb))+tssdh2(k,jb)
              voldh(jbdh(jb)) = voldh(jbdh(jb))+qdh2(k,jb)
              if (qdh2(k,jb) >= 0.0) then
                  qdh2_cum_plus(k,jb) = qdh2_cum_plus(k,jb) + qdh2(k,jb)*dlt
              else
                  qdh2_cum_minus(k,jb) = qdh2_cum_minus(k,jb) + qdh2(k,jb)*dlt
              end if
            end do
          end if
!<mueller>
!    <date>2001/02/06></date>
!    <reason>adding temperature (heat) to every element
!            analogous to gw q putting it layer with
!            corresponding density could be included
!    </reason>
!write(*,*) 'coupling'
        if (gw_coupling) then
            do i =iu,id
                do k = kt,kb(i)
                   tss(k,i)  = tss(k,i) + tgw(k,i)*qgwin(k,i)
                   tss(k,i)  = tss(k,i) + t2(k,i)*qgwout(k,i)
                   if (distributedGwTemperature) then
                        if (currentGwOutVolume(k,1) == 0) then
                            tlake(k,i) = t2(k,i)
                        else
                            tlake(k,i) = (tlake(k,i) * currentGwOutVolume(k,i) + &
                                            t2(k,i) * qgwout(k,i) * dlt) / &
                                            (currentGwOutVolume(k,i) + qgwout(k,i) * dlt)
                        end if
                        currentGwOutVolume(k,i) = currentGwOutVolume(k,i) + qgwout(k,i) * dlt
                   end if
                   volgwin(jb) = volgwin(jb) + qgwin(k,i)*dlt
                   volgwout(jb) = volgwout(jb) + qgwout(k,i)*dlt
                   tssgwin(jb) = tssgwin(jb) + tgw(k,i)*qgwin(k,i)
                   tssgwout(jb) = tssgwout(jb) + t2(k,i)*qgwout(k,i)
                end do
                if (oldkt < kt) then
                    tlake(kt,i) = (tlake(kt,i) * currentGwOutVolume(kt,i) + &
                                   tlake(oldkt,i) * currentGwOutVolume(oldkt,i)) / &
                                   (currentGwOutVolume(kt,i) + currentGwOutVolume(oldkt,i))
                    oldkt = kt
                end if
            end do
        end if

        !<date>2013/08/20</date>
        if (loading) then
            do i =iu,id
                do k = kt,kb(i)
                   tss(k,i)  = tss(k,i) + tload(k,i) * qload(k,i)
                   volload(jb) = volload(jb) + qload(k,i) * dlt
                   tssload(jb) = tssload(jb) + tload(k,i) * qload(k,i)
                end do
            end do
        end if
!</mueller>
!******** horizontal advection and diffusion multipliers

          do i=iu,id-1
            do k=kt,kb(i)
              cour = u(k,i)*dlt/dlxr(i)
              if (u(k,i).ge.0.0) then
                t1l = t2(k,i-1)
                t2l = t2(k,i)
                t3l = t2(k,i+1)
                if (u(k,i-1).le.0.0) t1l = t2(k,i)
                if (upwind) then
                  dx1(k,i)  = 0.0d0
                  dx2(k,i)  = -dx(k,i)/sf1l(k,i)
                  dx3(k,i)  = dx(k,i)/sf1l(k,i)
                  ad1l(k,i) = 0.0d0
                  ad2l(k,i) = 1.0d0
                  ad3l(k,i) = 0.0d0
                else
                  dx1(k,i)  = dx(k,i)*sf11l(k,i,1)
                  dx2(k,i)  = dx(k,i)*sf12l(k,i,1)
                  dx3(k,i)  = dx(k,i)*sf13l(k,i,1)
                  alfa      = 2.0*(dx(k,i)*dlt/(sf1l(k,i)*sf1l(k,i))    &
     &                        -(1.0d0-cour*cour)/6.0)*sf3l(k,i,1)
                  ad1l(k,i) = (alfa-cour*sf8l(k,i,1)*0.5d0)/sf5l(k,i,1)
                  ad2l(k,i) = sf4l(k,i,1)+(alfa-cour*sf9l(k,i,1)*0.5d0)    &
     &                        /sf6l(k,i,1)
                  ad3l(k,i) = sf2l(k,i,1)+(alfa-cour*sf10l(k,i,1)*0.5d0)    &
     &                        /sf7l(k,i,1)
                end if
              else
                t1l = t2(k,i)
                t2l = t2(k,i+1)
                t3l = t2(k,i+2)
                if (u(k,i+2).ge.0.0) t3l = t2(k,i+1)
                if (upwind) then
                  dx1(k,i)  = -dx(k,i)/sf1l(k,i)
                  dx2(k,i)  = dx(k,i)/sf1l(k,i)
                  dx3(k,i)  = 0.0d0
                  ad1l(k,i) = 0.0d0
                  ad2l(k,i) = 1.0d0
                  ad3l(k,i) = 0.0d0
                else
                  dx1(k,i)  = dx(k,i)*sf11l(k,i,2)
                  dx2(k,i)  = dx(k,i)*sf12l(k,i,2)
                  dx3(k,i)  = dx(k,i)*sf13l(k,i,2)
                  alfa      = 2.0*(dx(k,i)*dlt/(sf1l(k,i)*sf1l(k,i))    &
     &                        -(1.0d0-cour*cour)/6.0)*sf3l(k,i,2)
                  ad1l(k,i) = sf2l(k,i,2)+(alfa-cour*sf8l(k,i,2)*0.5d0)    &
     &                        /sf5l(k,i,2)
                  ad2l(k,i) = sf4l(k,i,2)+(alfa-cour*sf9l(k,i,2)*0.5d0)    &
     &                        /sf6l(k,i,2)
                  ad3l(k,i) = (alfa-cour*sf10l(k,i,2)*0.5d0)/sf7l(k,i,2)
                end if
              end if
              tadl(k,i) =  (dx1(k,i)-u(k,i)*ad1l(k,i))*t1l                &
     &                    +(dx2(k,i)-u(k,i)*ad2l(k,i))*t2l                &
     &                    +(dx3(k,i)-u(k,i)*ad3l(k,i))*t3l
            end do
          end do

!******** vertical advection multipliers

          do i=iu,id
            do k=kt,kb(i)-1
              if (w(k,i).ge.0.0) then
                t1v = t2(k-1,i)
                t2v = t2(k,i)
                t3v = t2(k+1,i)
                if (k.le.kt+1) then
                  t1v  = t2(kt,i)
                  htop = hkt1(i)
                  hbot = h(k+1)
                  hmid = h(k)
                  if (k.eq.kt) hmid = hkt1(i)
                  hmin       = min(hbot,hmid)
                  sf1v(k)    = (hbot+hmid)*0.5d0
                  sf2v(k,1)  = hmid**2
                  sf3v(k,1)  = hmid/(hmid+hbot)
                  sf4v(k,1)  = hbot/(hmid+hbot)
                  sf5v(k,1)  = 0.25*(htop+2.0*hmid+hbot)*(htop+hmid)
                  sf6v(k,1)  = -0.25*(hmid+hbot)*(htop+hmid)
                  sf7v(k,1)  = 0.25*(hmid+hbot)*(htop+2.0*hmid+hbot)
                  sf8v(k,1)  = 0.5d0*(hmid-hbot)*hmin
                  sf9v(k,1)  = 0.5d0*(htop+2.0*hmid-hbot)*hmin
                  sf10v(k,1) = 0.5d0*(htop+3.0*hmid)*hmin
                end if
                if (upwind) then
                  ad1v(k,i) = 0.0d0
                  ad2v(k,i) = 1.0d0
                  ad3v(k,i) = 0.0d0
                else
                  cour      = w(k,i)*dlt/sf1v(k)
                  alfa      = 2.0*(dzq(k,i)*dlt/(sf1v(k)*sf1v(k))        &
     &                        -(1.0d0-cour*cour)/6.0)*sf2v(k,1)
                  ad1v(k,i) = (alfa-cour*sf8v(k,1)*0.5d0)/sf5v(k,1)
                  ad2v(k,i) = sf4v(k,1)+(alfa-cour*sf9v(k,1)*0.5d0)        &
     &                        /sf6v(k,1)
                  ad3v(k,i) = sf3v(k,1)+(alfa-cour*sf10v(k,1)*0.5d0)        &
     &                        /sf7v(k,1)
                end if
              else
                t1v = t2(k,i)
                t2v = t2(k+1,i)
                t3v = t2(k+2,i)
                if (k.eq.kb(i)-1) t3v = t2(k+1,i)
                if (k.eq.kt) then
                  htop       = hkt1(i)
                  hmid       = h(kt+1)
                  hbot       = h(kt+2)
                  hmin       = min(htop,hmid)
                  sf1v(k)    = (hmid+htop)*0.5d0
                  sf2v(k,2)  = hmid**2
                  sf3v(k,2)  = hmid/(htop+hmid)
                  sf4v(k,2)  = htop/(htop+hmid)
                  sf5v(k,2)  = 0.25*(htop+2.0*hmid+hbot)*(htop+hmid)
                  sf6v(k,2)  = -0.25*(hmid+hbot)*(htop+hmid)
                  sf7v(k,2)  = 0.25*(htop+2.0*hmid+hbot)*(hmid+hbot)
                  sf8v(k,2)  = -0.5d0*(3.0*hmid+hbot)*hmin
                  sf9v(k,2)  = 0.5d0*(htop-2.0*hmid-hbot)*hmin
                  sf10v(k,2) = 0.5d0*(htop-hmid)*hmin
                end if
                if (upwind) then
                  ad1v(k,i) = 0.0d0
                  ad2v(k,i) = 1.0d0
                  ad3v(k,i) = 0.0d0
                else
                  cour      = w(k,i)*dlt/sf1v(k)
                  alfa      = 2.0*(dzq(k,i)*dlt/(sf1v(k)*sf1v(k))        &
     &                        -(1.0d0-cour*cour)/6.0)*sf2v(k,2)
                  ad1v(k,i) = sf3v(k,2)+(alfa-cour*sf8v(k,2)*0.5d0)        &
     &                        /sf5v(k,2)
                  ad2v(k,i) = sf4v(k,2)+(alfa-cour*sf9v(k,2)*0.5d0)        &
     &                        /sf6v(k,2)
                  ad3v(k,i) = (alfa-cour*sf10v(k,2)*0.5d0)/sf7v(k,2)
                end if
              end if
              tadv(k,i) = -w(k,i)*(ad1v(k,i)*t1v+ad2v(k,i)*t2v            &
     &                    +ad3v(k,i)*t3v)
            end do
          end do
        end do



!$message:'      transport'
!****** heat transport
!write(*,*) 'heat'
        do jb=1,nbp
          iu = cus(jb)
          id = ds(jb)
          do i=iu,id
            theta = thetai
            if (one_layer(i)) theta = 0.0d0
            t1(kt,i) = (t2(kt,i)*bhkt2(i)/dlt+(tadl(kt,i)*bhrkt1(i)        &
     &                 -tadl(kt,i-1)*bhrkt1(i-1))/dlx(i)+(1.0d0-theta)    &
     &                 *tadv(kt,i)*bb(kt,i)+tss(kt,i)/dlx(i))*dlt        &
     &                 /bhkt1(i)
            do k=kt+1,kb(i)
              t1(k,i) = (t2(k,i)*bh(k,i)/dlt+(tadl(k,i)*bhr(k,i)        &
     &                  -tadl(k,i-1)*bhr(k,i-1))/dlx(i)+(1.0d0-theta)        &
     &                  *(tadv(k,i)*bb(k,i)-tadv(k-1,i)*bb(k-1,i))        &
     &                  +tss(k,i)/dlx(i))*dlt/bh(k,i)


            end do

!********** vertical advection and implicit diffusion

            if (.not.one_layer(i)) then
              k       = kt
              at(k,i) = 0.0d0
              ct(k,i) = dlt/bhkt1(i)*(bb(k,i)*(theta*0.5d0*w(k,i)-dz(k,i)    &
     &                  /avhkt(i)))
              vt(k)   = 1.0d0+dlt/bhkt1(i)*(bb(k,i)*(dz(k,i)/avhkt(i)        &
     &                  +theta*0.5d0*w(k,i)))
              dt(k)   = t1(k,i)
              k       = kt+1
              at(k,i) = -dlt/bh(k,i)*(bb(k-1,i)*(dz(k-1,i)/avhkt(i)        &
     &                  +theta*0.5d0*w(k-1,i)))
              ct(k,i) = dlt/bh(k,i)*(bb(k,i)*(theta*0.5d0*w(k,i)-dz(k,i)    &
     &                  /avh(k)))
              vt(k)   = 1.0d0+dlt/bh(k,i)*(bb(k,i)*(dz(k,i)/avh(k)+theta    &
     &                  *0.5d0*w(k,i))+bb(k-1,i)*(dz(k-1,i)/avhkt(i)        &
     &                  -theta*0.5d0*w(k-1,i)))
              dt(k)   = t1(k,i)
              do k=kt+2,kb(i)-1
                at(k,i) = -dlt/bh(k,i)*(bb(k-1,i)*(dz(k-1,i)/avh(k-1)    &
     &                    +theta*0.5d0*w(k-1,i)))
                ct(k,i) = dlt/bh(k,i)*(bb(k,i)*(theta*0.5d0*w(k,i)        &
     &                      -dz(k,i)/avh(k)))
                vt(k)   = 1.0d0+dlt/bh(k,i)*(bb(k,i)*(dz(k,i)/avh(k)        &
     &                      +theta*0.5d0*w(k,i))+bb(k-1,i)*(dz(k-1,i)        &
     &                      /avh(k-1)-theta*0.5d0*w(k-1,i)))
                dt(k)   = t1(k,i)
              end do
              k = kb(i)
              if (kb(i)-kt.gt.1) then
                at(k,i) = -dlt/bh(k,i)*(bb(k-1,i)*(dz(k-1,i)/avh(k-1)    &
     &                    +theta*0.5d0*w(k-1,i)))
                ct(k,i) = 0.0d0
                vt(k)   = 1.0d0+dlt/bh(k,i)*(bb(k-1,i)*(dz(k-1,i)            &
     &                      /avh(k-1)-theta*0.5d0*w(k-1,i)))
                dt(k)   = t1(k,i)
              else
                at(k,i) = -dlt/bh(k,i)*(bb(k-1,i)*(dz(k-1,i)/avhkt(i)    &
     &                    +theta*0.5d0*w(k-1,i)))
                ct(k,i) = 0.0d0
                vt(k)   = 1.0d0+dlt/bh(k,i)*(bb(k-1,i)*(dz(k-1,i)            &
     &                      /avhkt(i)-theta*0.5d0*w(k-1,i)))
                dt(k)   = t1(k,i)
              end if

!************ tridiagonal solution

              btat(kt,i) = vt(kt)
              do k=kt+1,kb(i)
                btat(k,i) = vt(k)-at(k,i)/btat(k-1,i)*ct(k-1,i)
              end do
              gmat(kt) = dt(kt)
              do k=kt+1,kb(i)
                gmat(k) = dt(k)-at(k,i)/btat(k-1,i)*gmat(k-1)

              end do
              t1(kb(i),i) = gmat(kb(i))/btat(kb(i),i)
              do k=kb(i)-1,kt,-1
                t1(k,i) = (gmat(k)-ct(k,i)*t1(k+1,i))/btat(k,i)
              end do
            end if
          end do
        end do
        vactive = 0.0d0
    hactive = 0.0d0
    do jb=1,nbp
       iu = cus(jb)
       id = ds(jb)
       do i=iu,id
         do k=kt+1,kb(i)
            vactive(k,i) = bh(k,i) * dlx(i)
            hactive(k,i) = h(k)
         enddo
         vactive(kt,i) = bhkt2(i) * dlx(i)
         hactive(kt,i) = hkt2(i)
       enddo
    enddo
    do i=1,imp
       elws(i) = el(kt)-z(i)
    end do

        !write(*,*) 'end'
!if (year > 2006 .or. jday < 1.3) write(*,*) 't1 in hydrodynamics end', t1
    end subroutine