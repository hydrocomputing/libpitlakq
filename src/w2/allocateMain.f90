    subroutine allocate_main
      use shared_data
      implicit none
      allocate(cunit3(ncp))
      cunit3='g '
      allocate(acc(ncp), inacc(ncp), tracc(ncp), dtacc(ncp), pracc(ncp))
      allocate(dtrc(ncp), swc(ncp))
      allocate(cunit2(ncp))
      cunit2=('g/m^3')
      allocate(cname1(ncp))
      allocate(segment(imp))
      allocate(trc(ntp))
!      allocate(rsifn(nbp),  vprfn(nbp), tsrfn(nbp), prffn(nbp), &
!     &               vplfn(nbp), cplfn(nbp), snpfn(nbp),    &
!     &             lprfn(nbp),  bthfn(nbp), sprfn(nbp))
      allocate(sink(nsp,nbp))
      allocate(conv1(kmp,imp))
            cname1     =(/'tracer          ',                &
     &                    'suspended_solids',                &
     &                    'coliform        ',                &
     &                    'dissolved_solids',                &
     &                    'labile_dom      ',                &
     &                    'refractory_dom  ',                &
     &                    'algae           ',                &
     &                    'labile pom      ',                &
     &                    'phosphorus      ',                &
     &                    'ammonium        ',                &
     &                    'nitrate-nitrite ',                &
     &                    'dissolved_oxygen',                &
     &                    'sediment        ',                &
     &                    'inorganic_carbon',                &
     &                    'alkalinity      ',                &
     &                    'ph              ',                &
     &                    'carbon_dioxide  ',                &
     &                    'bicarbonate     ',                &
     &                    'carbonate       ',                &
     &                    'iron            ',                &
     &                    'cbod            ',                &
     &                    'aluminium       ',                & ! mueller
     &                    'sulfate         ',                & ! mueller
     &                    'calcium         ',                & ! mueller
     &                    'magnesium       ',                & ! mueller
     &                    'sodium          ',                & ! mueller
     &                    'potassium       ',                & ! mueller
     &                    'manganese       ',                & ! mueller
     &                    'chlorid         ',                & ! mueller
     &                    'iron iii        ',                & ! mueller
     &                    'manganese iii   ',                & ! mueller
     &                    'sulfide         ',                & ! mueller
     &                    'methane         ',                & ! mueller
     &                    'iron hydroxide  ',                & ! mueller
     &                    'al: hydroxide   ',                & ! mueller
     &                    'cadmium         ',                & ! mueller
     &                    'lead            ',                & ! mueller
     &                    'zinc            ',                & ! mueller
     &                    'arsenic         '/)                 ! mueller



!      allocate (cn0(ncp))
      allocate (skti(imp), kbmin(imp))
      allocate (iprf(imp), ispr(imp))
      allocate (ipri(imp))
      allocate (iwd(nwp), kwd(nwp), jbwd(nwp))
      allocate (jbtr(ntp), itr(ntp), kttr(ntp), kbtr(ntp))
      allocate (ktqin(nbp), kbqin(nbp))
      allocate (jbuh(nbp), jbdh(nbp))
      allocate (uhs(nbp), dhs(nbp), us(nbp), nl(nbp))



      allocate (dn_head(nbp),up_head(nbp))
      allocate (ice_in(nbp))
      allocate (allow_ice(imp),    one_layer(imp))
      allocate (iso_conc(ncp), vert_conc(ncp), long_conc(ncp))
      allocate (transport(ncp))
      allocate (place_qtr(ntp), specify_qtr(ntp))
      allocate (trib_specie_balance(ntp,ncp))


!**** dimension statements


      allocate (icesw(imp))
      allocate (bhrkt2(imp), avhkt(imp),  dlx(imp), dlxr(imp), elws(imp))
      allocate (dlxrho(imp))
      allocate (ev(imp), qdt(imp), qpr(imp))
      allocate (q(imp), qc(imp), qssum(imp))
      allocate (sods(imp))
      allocate (rn(imp), phi0(imp))
      allocate (avh(kmp))
      !<mueller>
!    <date>2001/02/06></date>
!    <reason>void</reason>
      allocate (w2z(kmp))
!</mueller>
      allocate (depthb(kmp))
      allocate (tvp(kmp))
      allocate (sf1v(kmp))
      allocate (twd(nwp))
      allocate (eltrt(ntp), eltrb(ntp))
      allocate (c2i(ncp), avcout(ncp))
      allocate (qprbr(nbp),  evbr(nbp))
      allocate (snpd(ndp), vpld(ndp), prfd(ndp), cpld(ndp), rsod(ndp), tsrd(ndp), dltd(ndp), &
     &                          wscd(ndp), sprd(ndp), scrd(ndp))
      allocate (snpf(ndp), vplf(ndp), prff(ndp), cplf(ndp), rsof(ndp), tsrf(ndp),  &
     &                          dltf(ndp), scrf(ndp), sprf(ndp))
      allocate (dltmax(ndt))
!<mueller>
!    <date>2001/02/06></date>
!    <reason>void</reason>
      allocate (w2x(imp))

      allocate (sf2v(kmp,2), sf3v(kmp,2), sf4v(kmp,2), sf5v(kmp,2), sf6v(kmp,2),            &
     &          sf7v(kmp,2), sf8v(kmp,2), sf9v(kmp,2), sf10v(kmp,2))
      allocate (xdummy(kmc,imc))
!</mueller>

      allocate (vactive(kmc,imc))
      vactive = 0.0d0
      allocate (hactive(kmc,imc))
      hactive = 0.0d0
      allocate (balance_error(kmc,imc))
      balance_error = 0
      !<mueller>
!    <date>2001-02-06</date>
!    <reason>gw-inflow for all cells</reason>
      allocate (qgw(kmp,imp))
      allocate (qgwin(kmp,imp))
      allocate (qgwout(kmp,imp))
!    <date>2005-05-13</date>
!    <reason>gw and lake temperature for all cells</reason>
      allocate (tgw(kmp,imp))
      allocate (tlake(kmp,imp))
      allocate (currentGwOutVolume(kmp,imp))

      !<date>2013/08/20</date>
      !<date>2013/08/20</date>
      allocate (tload(kmp,imp), qload(kmp,imp))
!</mueller>
      allocate (admx(kmp,imp), admz(kmp,imp), dz(kmp,imp), dx(kmp,imp), &
     &             dm(kmp,imp), p(kmp,imp),    &
     &             hpg(kmp,imp), hdg(kmp,imp), sb(kmp,imp), st(kmp,imp),    dzq(kmp,imp))
      allocate (su(kmp,imp), sw(kmp,imp), saz(kmp,imp))
      allocate (tss(kmp,imp), qss(kmp,imp))
      allocate (bb(kmp,imp), br(kmp,imp), bhr(kmp,imp), hseg(kmp,imp))
      allocate (sf1l(kmp,imp))
      allocate (cwd(ncp,nwp))
      allocate (quh1(kmp,nbp), quh2(kmp,nbp), qdh1(kmp,nbp), qdh2(kmp,nbp))
      allocate (quh1_cum_plus(kmp,nbp), quh2_cum_plus(kmp,nbp), qdh1_cum_plus(kmp,nbp), qdh2_cum_plus(kmp,nbp))
      allocate (quh1_cum_minus(kmp,nbp), quh2_cum_minus(kmp,nbp), qdh1_cum_minus(kmp,nbp), qdh2_cum_minus(kmp,nbp))
      allocate (tssuh1(kmp,nbp) , tssuh2(kmp,nbp) , tssdh1(kmp,nbp) , tssdh2(kmp,nbp) )
      allocate (qinf(kmp,nbp))
      allocate (qtrf(kmp,ntp))
      allocate (akbr(kmp,nbp))
      allocate (fetchu(imp,nbp), fetchd(imp,nbp))
      allocate (cvp(kmc,ncp))
      allocate (sf2l(kmp,imp,2), sf3l(kmp,imp,2), sf4l(kmp,imp,2), sf5l(kmp,imp,2), sf6l(kmp,imp,2),        &
     &          sf7l(kmp,imp,2), sf8l(kmp,imp,2), sf9l(kmp,imp,2), sf10l(kmp,imp,2), &
     &            sf11l(kmp,imp,2), sf12l(kmp,imp,2), sf13l(kmp,imp,2))
      allocate (cssuh1(kmc,ncp,nbp) , cssuh2(kmc,ncp,nbp) , cssdh1(kmc,ncp,nbp) , cssdh2(kmc,ncp,nbp))
      allocate (c1s(kmc,imc,ncp),  cssb(kmc,imc,ncp))

!<mueller>
!    <date>2001/02/06</date>
!    <reason> keeping track of gw volume</reason>
     if (.not.restart) then
      allocate (volgwin(nbp), volgwout(nbp), tssgwin(nbp), tssgwout(nbp))
      !<date>2013/08/20</date>
      allocate (volload(nbp), tssload(nbp))
      allocate (tssev(nbp),  tsspr(nbp),  tsstr(nbp),        &
                tssdt(nbp),  tsswd(nbp),  tssuh(nbp),    &
     &          tssdh(nbp),  tssin(nbp),  tssout(nbp), tsss(nbp), &
     &            tssb(nbp),   tssice(nbp),    &
     &          esbr(nbp),   etbr(nbp),   eibr(nbp))

      allocate (volsbr(nbp), voltbr(nbp), volev(nbp), volpr(nbp),    &
                voltr(nbp), voldt(nbp), volwd(nbp),  voluh(nbp),  voldh(nbp), &
    &           volin(nbp),  volout(nbp), dlvol(nbp), volibr(nbp))
     allocate (csstr(ncp,nbp), cssdt(ncp,nbp), csswd(ncp,nbp), csspr(ncp,nbp),     &
              cssin(ncp,nbp), cssout(ncp,nbp), cssuh(ncp,nbp), cssdh(ncp,nbp),    &
              cssphc(ncp,nbp), cssw2(ncp,nbp), cssgwin(ncp,nbp), cssgwout(ncp,nbp), &
              cssatm(ncp,nbp), csssed(ncp,nbp), cssload(ncp,nbp))
     end if

      allocate (bhrho(imp), gma(imp), bta(imp), a(imp), c(imp), d(imp),        &
     &                                       v(imp), f(imp))
      allocate (sz(imp))
      allocate (gmat(kmp),   dt(kmp),     vt(kmp))
      allocate (btat(kmp,imp),    ct(kmp,imp),  at(kmp,imp))
      allocate (tadl(kmp,imp),   tadv(kmp,imp))
      allocate (ad1l(kmp,imp),   ad2l(kmp,imp),   ad3l(kmp,imp),        &
     &            ad1v(kmp,imp),   ad2v(kmp,imp),   ad3v(kmp,imp))
      allocate (dx1(kmp,imp),    dx2(kmp,imp),    dx3(kmp,imp))
      allocate (cmbrs(ncp,nbp),  cmbrt(ncp,nbp))
      allocate (cadl(kmc,imc,ncp),  cadv(kmc,imc,ncp))


    end subroutine allocate_main
