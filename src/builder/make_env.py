
import json
import os
from subprocess import run
import sys


def env_exists(env_name):
    """Find out if a conda environment exits."""
    info = json.loads(
        run(
        ['conda', 'info', '--env', '--json'],
        check=True,
        capture_output=True,
        text=True).stdout
        )
    for env in info['envs']:
        if env.rsplit(os.sep, 1)[-1] == env_name:
            return True
    return False


def create_env_name(py_version, arch=None):
    """Create a name for an env."""
    name = 'libpitlakq_compile_'
    if arch:
        name += arch.replace('-', '_') + '_'
    name += f'py{str(py_version).replace(".", "")}'
    return name


def create_conda_env(py_version=None, arch=None, name=None):
    """Create a conda environment for compilation."""
    if name is None:
        name = create_env_name(py_version, arch)
    env = None
    if not env_exists(name):
        if arch:
            env = os.environ.copy()
            env['CONDA_SUBDIR'] = arch
        cmd = [
            'mamba', 'create', '-y', '-n', name,
            f'python={py_version}',
            'numpy']
        if sys.platform != 'win32':
            cmd.append('gfortran')
        run(cmd, check=True, env=env, capture_output=True)
        if arch:
            run(['conda', 'config', '--env', '--set', 'subdir', arch])
    return name


def main():
    """
    Create a new conda environment.

    Python version and architecture are command line args.
    """
    arch = None
    args = sys.argv[1:]
    py_version = args[0]
    if len(args) > 1:
        arch = args[1].strip()
        if arch not in ['osx-64']:
            arch = None
    print(create_conda_env(py_version=py_version, arch=arch))

if __name__ == '__main__':
    main()
