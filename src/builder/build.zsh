#!/bin/zsh

eval "$(conda shell.bash hook)"

for version in 3.9 3.10 3.11
do
    for arch in osx-64 osx-arm64
    do
        name="$(python make_env.py $version $arch)"
        langarch="$()"
        if [ $(arch) = 'osx-64' ]; then
           langarch = "ARCH=x86_64"
        fi
        conda activate $name
        CC=clang $langarch python compile_w2.py config --fcompiler=gfortran build
        conda deactivate
    done
done
