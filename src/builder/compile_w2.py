#!/usr/bin/env python


import os
import platform
from subprocess import run
import sys


from numpy.distutils.core import Extension
from numpy.distutils.core import setup

os.environ["CC"] = 'gcc'

SOURCE_FILE_NAMES = [
    'kinetics.f90',
    'heat_exchange.f90',
    'hydrodynamics.f90',
    'close_files.f90',
    'deallocate_pointers.f90',
    'update_kb.f90',
    'read_input.f90',
    'set_pointers.f90',
    'constituents.f90',
    'time_varying_data.f90',
    'allocateMain.f90',
    'cleanup.f90',
    'co2sat.f90',
    'deallocate_main.f90',
    'density.f90',
    'initVariables.f90',
    'miscCalculations.f90',
    'selective_withdrawal.f90',
    'setallzero.f90',
    'shared_data.f90',
    'shared_data_kinetics.f90',
    'times.f90'
    ]

SIGN_FILE_NAME = 'w2.pyf'

def make_source_paths(
        dir_name='w2',
        sources=SOURCE_FILE_NAMES):
    """Create list for source code file names with directory."""
    return [f'../{dir_name}/{source}' for source in sources]


def sign(sources, sign_file_name=SIGN_FILE_NAME,):
    """Create f2py sign file."""
    cmd = [
        'f2py',
        '-m', 'w2_fortran',
        '--overwrite-signature',
        '-h', sign_file_name]
    cmd.extend(sources)
    run(cmd)
    return [sign_file_name] + sources


def build():
    """Build the extension."""
    sources = make_source_paths()
    sources = sign(sources)

    f2py_options = []
    extra_f77_compile_args = []
    extra_f90_compile_args = []
    extra_link_args = []
    extra_compile_args = []

    if sys.platform == 'win32':
        extra_f77_compile_args = ['-DMS_WIN64', '-fallow-argument-mismatch']
        extra_f90_compile_args = ['-DMS_WIN64', '-fallow-argument-mismatch']
        extra_link_args = ['-DMS_WIN64']
        extra_compile_args = [
                '-Wno-incompatible-pointer-types',
                ]
    elif sys.platform == 'darwin':
        machine = platform.uname().machine
        extra_compile_args = [
                '-Wno-incompatible-pointer-types',
                ]
        extra_f77_compile_args = ['-m64', '-Wall', '-undefined dynamic_lookup',
                                '-bundle', '-fallow-argument-mismatch']
        extra_f90_compile_args = ['-m64', '-Wall', '-undefined dynamic_lookup',
                                '-bundle', '-fallow-argument-mismatch']
        if machine == 'x86_64':
            extra_f90_compile_args.extend(['-march=x86-64'])
            extra_f77_compile_args.extend(['-march=x86-64'])
        elif machine == 'arm64':
            pass
    elif sys.platform == 'linux':
        extra_f77_compile_args = ['-fPIC', '-fallow-argument-mismatch']
        extra_f90_compile_args = ['-fPIC', '-fallow-argument-mismatch']
    try:
        ext = Extension(name='w2_fortran',
                        sources=sources,
                        extra_f77_compile_args=extra_f77_compile_args,
                        extra_f90_compile_args=extra_f90_compile_args,
                        extra_link_args=extra_link_args,
                        extra_compile_args=extra_compile_args,
                        f2py_options=f2py_options)
    except TypeError:
    # Old versions of f2py do not have the `extra_` keyword arguments.
        print('Using old f2py version without flags starting with "extra_".')
        ext = Extension(name='w2_fortran',
                        sources=sources,
                        f2py_options=f2py_options)


    setup(name = 'w2_fortran',
        description       = "CE-QUAL-W2 v2.0 coupled",
        author            = "Mike Mueller",
        author_email      = "mmueller@hydrocomputing.com",
        ext_modules = [ext]
        )

if __name__ == '__main__':
    build()