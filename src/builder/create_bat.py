"""
Create batch file for compilation

A batch file is need to use `call conda.bat activate` that activates a
conda environment inside the currnet process.
Since looping in batch file has very strange effecs in terms of varible
scoping, gneretaing a tach file seems the easiest and savest solution here.
"""


import sys

from make_env import create_env_name


VERSIONS = ['3.9', '3.10', '3.11']


def create_bat(file_name='build.bat', version=VERSIONS):
    """
    Create batch file to avoid looping.
    """
    lines = ['@Echo off']
    lines += ['rem auto generated, do not edit, your changes will be lost\n']
    for version in version:
        lines.append(f'{sys.executable} make_env.py {version}')
        name = create_env_name(version)
        lines.append(f'call conda.bat activate {name}')
        lines.append('python --version')
        lines.append('python compile_w2.py build_ext --fcompiler=gfortran --compiler=mingw32' )
        lines.append(f'call conda.bat activate base\n')
    with open(file_name, 'w') as fobj:
        fobj.write('\n'.join(lines))


if __name__ == '__main__':
    create_bat()