#!/bin/sh

for version in 3.9 3.10 3.11
do
    name="$(python make_env.py $version)"
    conda activate $name
    CC=gcc python compile_w2.py config --fcompiler=gfortran build
    conda deactivate
done
