#!/usr/bin/env python
"""Create and release conda packages and wheels."""

from contextlib import chdir
from pathlib import Path
import os
import platform
import shutil
from string import Template
from subprocess import run, CalledProcessError
from textwrap import dedent


class DirsCreator:
    """
    Create directory structure for a version and platform.

    Fill out the templates on the way.
    """

    MSYS64_ROOT = Path(r'C:\msys64\mingw64\bin')
    WIN_DLLS = [
        'libwinpthread-1.dll',
        'libgcc_s_seh-1.dll',
        'libgfortran-5.dll',
        'libquadmath-0.dll',
    ]

    def __init__(
        self,
        version,
        py_version,
        numpy_version='1.24',
        lib_name='libpitlakq',
        root_path=Path('../..'),
        base_path=Path('../dists'),
        template_path=Path('../templates'),
        build_dir=Path('build'),
        upload=True,
    ):
        self.version = version
        self.py_version = py_version
        self.numpy_version = numpy_version
        self.py_version_name = py_version.replace('.', '')
        self.lib_name = lib_name
        self.root_path = root_path
        self.base_path = base_path
        self.template_path = template_path
        self.build_dir = build_dir
        self.upload = upload
        self.version_path = None
        self.version_src_path = None
        platform_name_mapping = {'windows': 'win', 'darwin': 'macosx'}
        self.mac_compiler_version_mapping = {'arm64': '11.0', 'x86_64': '10.9'}
        system = platform.uname().system.lower()
        self.platform_name = platform_name_mapping.get(system, system)
        self.machine = platform.uname().machine.lower()
        self.os_machine_key = f'{self.platform_name}_{self.machine}'
        platform_file_names = {
            f'win_{self.machine}': Path(
                f'w2_fortran.cp{self.py_version_name}-win_{self.machine}.pyd'
            ),
            f'linux_{self.machine}': Path(
                f'w2_fortran.cpython-{self.py_version_name}-{self.machine}-linux-gnu.so'
            ),
            f'macosx_{self.machine}': (
                f'w2_fortran.cpython-{self.py_version_name}-darwin.so'
            ),
        }
        self.binary_name = platform_file_names[self.os_machine_key]
        if system == 'darwin':
            compiler_version = (
                '-' + self.mac_compiler_version_mapping[self.machine]
            )
            self.sub_dir = {'x86_64': 'osx-64', 'arm64': 'osx-arm64'}[
                self.machine
            ]
        else:
            compiler_version = ''
        self.binary_path_name = Path(
            f'lib.{self.platform_name}{compiler_version}-{self.machine}-cpython-{self.py_version_name}'
        )

    def create_dirs(self):
        """Create version directory."""
        platform_path = self.base_path / self.os_machine_key
        platform_path.mkdir(exist_ok=True)
        version_path = platform_path / f'py{self.py_version_name}'
        version_path.mkdir(exist_ok=True)
        self.version_path = version_path
        self.version_src_path = self.version_path / 'src'

    def copy_lib(self):
        """Copy Python source code and binary."""
        src = self.template_path / self.lib_name
        dst = self.version_src_path / self.lib_name
        shutil.copytree(src, dst, dirs_exist_ok=True)
        bin_src = self.build_dir / self.binary_path_name / self.binary_name
        bin_dst = dst / self.binary_name
        shutil.copy(bin_src, bin_dst)
        if self.platform_name == 'win':
            for name in self.WIN_DLLS:
                src = self.MSYS64_ROOT / name
                dst_file = dst / name
                shutil.copy(src, dst_file)

    def copy_meta(self):
        """Copy meta datat for conda-build."""
        for name in ['pyproject.toml', 'README.md']:
            src = self.root_path / name
            dst = self.version_path / name
            shutil.copy(src, dst)

    def amend_pyproject_toml(self):
        """Add variable infos to pyproject.toml."""
        src = self.version_path / 'pyproject.toml'
        dst = src
        text = src.read_text()
        text = Template(text).substitute({'version': self.version})
        if self.platform_name == 'win':
            package_data = """
            [tool.setuptools.package-data]
            libpitlakq = ["*.pyd", "*.dll"]
            """
        else:
            package_data = """
            [tool.setuptools.package-data]
            libpitlakq = ["*.so"]
            """
        text += dedent(package_data)
        dst.write_text(text)

    def copy_conda_package(self, dir_name='conda_package'):
        """Copy data for conda package."""
        src_path = self.template_path / dir_name
        dst_path = self.version_path / dir_name
        dst_path.mkdir(exist_ok=True)
        makefile_name = 'Makefile'
        mf_content = (src_path / makefile_name).read_text()
        if self.upload:
            user = '--user=hydrocomputing'
        else:
            user = ''
        (dst_path / makefile_name).write_text(
            mf_content.format(
                sub_dir=self.sub_dir,
                py_version=self.py_version,
                numpy_version=self.numpy_version,
                user=user,
            )
        )
        lib_src_path = src_path / self.lib_name
        lib_dist_path = dst_path / self.lib_name
        lib_dist_path.mkdir(exist_ok=True)
        for name in ['bld.bat', 'build.sh']:
            src = lib_src_path / name
            dst = lib_dist_path / name
            shutil.copy(src, dst)
        meta_name = 'meta.yaml'
        meta_content = (lib_src_path / meta_name).read_text()
        (lib_dist_path / meta_name).write_text(
            meta_content.format(
                version=self.version, py_version=self.py_version
            )
        )

    def process(self):
        """Do all steps."""
        self.create_dirs()
        self.copy_lib()
        self.copy_meta()
        self.amend_pyproject_toml()
        self.copy_conda_package()


class WheelMaker:
    """Create and upload a PyPi wheel."""

    def __init__(self, creator):
        self.creator = creator
        self.wheel_name = None
        self.wheel_path = None

    def build_wheel(self):
        """Build a wheel."""
        with chdir(self.creator.version_path):
            wheel = run(
                ['python', '-m', 'build'],
                check=True,
                capture_output=True,
                text=True,
            )
        self.wheel_name = wheel.stdout.splitlines()[-1].split()[-1]

    def _rename_wheel(self, wheel_name=None):
        if wheel_name is None:
            wheel_name = self.wheel_name
        path = self.creator.version_path / 'dist'
        machine = self.creator.machine
        platform = self.creator.platform_name
        py_version_name = self.creator.py_version_name
        new_parts = f'cp{py_version_name}-' * 2
        if platform == 'win':
            new_parts += f'{platform}_{machine}'
        elif platform == 'linux':
            new_parts += f'manylinux2014_{machine}'
        elif platform == 'macosx':
            compiler = self.creator.mac_compiler_version_mapping[machine]
            compiler_string = compiler.replace('.', '_')
            new_parts += f'{platform}_{compiler_string}_{machine}'
        new_name = wheel_name.replace('py3-none-any', new_parts)
        src = path / wheel_name
        dst = path / new_name
        shutil.copy(src, dst)
        self.wheel_path = dst

    def upload_to_pypi(self, wheel_path=None, test=False):
        """Upload wheel to PyPi."""
        if wheel_path is None:
            wheel_path = str(self.wheel_path)
        cmd = ['twine', 'upload']
        if test:
            cmd.extend(['-r', 'testpypi', '--verbose'])
        cmd.append(f'--config-file={os.getenv("PYPI_CONFIG_FILE")}')
        cmd.append(wheel_path)
        run(cmd, check=True)


class CondaMaker:
    """Create and upload a conda package."""

    def __init__(self, creator):
        self.creator = creator
        self.conda_file_name = None

    def build_conda(self):
        """
        Build a conda package.

        The a package is uploaded to anaconda.org if

            anaconda_upload: true

        in `.condarc`.
        """
        print(f'Platform: {self.creator.platform_name}')
        print('Creating conda package ...')
        with chdir(self.creator.version_path / 'conda_package'):
            run(['make'], check=True)


def release(
    version,
    py_version,
    make_wheel=True,
    make_conda=True,
    upload=True,
    testpypi=False,
):
    """Relase one version."""
    creator = DirsCreator(version=version, py_version=py_version, upload=upload)
    creator.process()
    if make_wheel:
        print('Releasing wheel ...')
        wheel_maker = WheelMaker(creator)
        wheel_maker.build_wheel()
        wheel_maker._rename_wheel()
        if upload:
            try:
                wheel_maker.upload_to_pypi(test=testpypi)
            except CalledProcessError:
                print('Upload to PyPi not successful. Continue ...')
    if make_conda:
        print('Releasing conda ...')
        conda_maker = CondaMaker(creator)
        conda_maker.build_conda()


def main(
    version,
    py_versions,
    make_wheel=True,
    make_conda=True,
    upload=True,
    testpypi=False,
):
    """Create and upload a PyPi and a Conda package for one Python version."""
    for py_version in py_versions:
        print(f'Releasing for Python {py_version} ...')
        release(
            version=version,
            py_version=py_version,
            make_wheel=make_wheel,
            make_conda=make_conda,
            upload=upload,
            testpypi=testpypi,
        )


if __name__ == '__main__':
    main(
        version='0.1.1',
        py_versions=['3.9', '3.10', '3.11'],
        upload=True,
        make_wheel=True,
        make_conda=True,
    )
