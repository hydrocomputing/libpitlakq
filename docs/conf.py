"""Sphinx configuration."""
project = "Compiled Fortan code of CE-QAUL-W2 for PITLAKQ"
author = "Mike Müller"
copyright = "2023, Mike Müller"
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx_click",
    "myst_parser",
]
autodoc_typehints = "description"
html_theme = "furo"
