# Compiled Fortan code of CE-QUAL-W2 for PITLAKQ

[![PyPI](https://img.shields.io/pypi/v/libpitlakq.svg)][pypi status]
[![Status](https://img.shields.io/pypi/status/libpitlakq.svg)][pypi status]
[![Python Version](https://img.shields.io/pypi/pyversions/libpitlakq)][pypi status]
[![License](https://img.shields.io/pypi/l/libpitlakq)][license]

[![Read the documentation at https://libpitlakq.readthedocs.io/](https://img.shields.io/readthedocs/libpitlakq/latest.svg?label=Read%20the%20Docs)][read the docs]
[![Tests](https://github.com/hydrocomputing/libpitlakq/workflows/Tests/badge.svg)][tests]
[![Codecov](https://codecov.io/gh/hydrocomputing/libpitlakq/branch/main/graph/badge.svg)][codecov]

[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)][pre-commit]
[![Black](https://img.shields.io/badge/code%20style-black-000000.svg)][black]

[pypi status]: https://pypi.org/project/libpitlakq/
[read the docs]: https://libpitlakq.readthedocs.io/
[tests]: https://github.com/hydrocomputing/libpitlakq/actions?workflow=Tests
[codecov]: https://app.codecov.io/gh/hydrocomputing/libpitlakq
[pre-commit]: https://github.com/pre-commit/pre-commit
[black]: https://github.com/psf/black

## Features

- Compiled fortan code of CE-QUAL-W2 for [PITLAKQ](https://www.pitlakq.com/)

## Requirements

- None
- Will be installed by [PITLAKQ](https://www.pitlakq.com/)

## Installation

You can install _Compiled Fortan code of CE-QAUL-W2 for PITLAKQ_ via [pip] from [PyPI]:

```console
$ pip install libpitlakq
```

## Usage

Please see the [Command-line Reference] for details.

## Contributing

Contributions are very welcome.
To learn more, see the [Contributor Guide].

## License

Distributed under the terms of the [MIT license][license],
_Compiled Fortan code of CE-QUAL-W2 for PITLAKQ_ is free and open source software.

## Issues

If you encounter any problems,
please [file an issue] along with a detailed description.

## Credits

This project was generated from [@cjolowicz]'s [Hypermodern Python Cookiecutter] template.

[@cjolowicz]: https://github.com/cjolowicz
[pypi]: https://pypi.org/
[hypermodern python cookiecutter]: https://github.com/cjolowicz/cookiecutter-hypermodern-python
[file an issue]: https://github.com/hydrocomputing/libpitlakq/issues
[pip]: https://pip.pypa.io/

<!-- github-only -->

[license]: https://github.com/hydrocomputing/libpitlakq/blob/main/LICENSE
[contributor guide]: https://github.com/hydrocomputing/libpitlakq/blob/main/CONTRIBUTING.md
[command-line reference]: https://libpitlakq.readthedocs.io/en/latest/usage.html
